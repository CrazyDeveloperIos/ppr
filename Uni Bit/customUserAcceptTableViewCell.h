//
//  customUserAcceptTableViewCell.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 6/20/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customUserAcceptTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageProfile;

@end
