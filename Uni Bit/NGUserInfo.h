//
//  NGUserInfo.h
//  Uni Bit
//
//  Created by Naz on 24.08.15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping.h"

@interface NGUserInfo : NSObject <EKMappingProtocol>
@property (strong, nonatomic) NSString *statusUser;
@property (strong, nonatomic) NSString *role;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *surname;
@property (strong, nonatomic) NSString *nickname;
@property (strong, nonatomic) NSString *birthdate;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *filePach;
@property (strong, nonatomic) NSString *admin_check;
@property (strong, nonatomic) NSString *card;
@property (strong, nonatomic) NSString *id_user;
@property (strong, nonatomic) NSString *denied;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *adminCheck;
@property (strong, nonatomic) NSString *bicycle;
@property (strong, nonatomic) NSString *bus;
@property (strong, nonatomic) NSString *car;
@property (strong, nonatomic) NSString *carexist;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *creditCard;
@property (strong, nonatomic) NSString *creditValidation;
@property (strong, nonatomic) NSString *licensec;
@property (strong, nonatomic) NSString *licensep;
@property (strong, nonatomic) NSString *moto;
@property (strong, nonatomic) NSString *other;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *points;
@property (strong, nonatomic) NSString *ship;
@property (strong, nonatomic) NSString *truck;
@property (strong, nonatomic) NSString *van;
@property (strong, nonatomic) NSURL *imageURL;

@end
