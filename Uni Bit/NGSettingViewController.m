//
//  settingViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/10/15.
//  Copyright (c) 2015 User. All rights reserved.
//
#import "NGStartViewController.h"
#import "NGSettingViewController.h"
#import "NGServerManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKBridgeAPIRequest.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SCLAlertView.h"
#import "NGMissionData.h"
#import <QuartzCore/QuartzCore.h>
#import "NGAccessToken.h"
#import "SWRevealViewController.h"


@interface NGSettingViewController () 
@property (weak, nonatomic) NSString* urlImageFacebook;
@property (weak, nonatomic) IBOutlet UIImageView *imageUser;



@end

@implementation NGSettingViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        self.sidebarButton.action = @selector(revealToggle:);
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)shareButton:(id)sender {
    NSString *link = [NSString stringWithFormat:@"https://vk.com/o_y_0"];
    NSString *noteStr = [NSString stringWithFormat:@"Hee!Go using good App, please follow the link below on your. %@", link];
    
    NSURL *url = [NSURL URLWithString:link];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[noteStr, url] applicationActivities:nil];
    [self presentViewController:activityVC animated:YES completion:nil];

}
- (IBAction)logoutButton:(id)sender {
    
    
    [[NGServerManager sharedManager] withClickLogout:nil
     
                                           onSuccess:^(NSDictionary *dict) {
                                               
                                               NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                                               [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                                               
                                               
                                               NSString * storyboardName = @"Main";
                                               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                                               UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
                                               //[self showViewController:vc sender:self];
                                               [self.navigationController presentViewController:vc animated:YES completion:nil];
                                               
                                               
                                               
                                           }
                                           onFailure:^(NSError *error) {
                                               NSLog(@"Error");
                                                }];
    
}

- (IBAction)chooseImageAction:(UIButton *)sender {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    //    imageViewTodo.image = image;
    //    isPicChanged = YES;
    //    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end
