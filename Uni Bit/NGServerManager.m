//
//  NGServerManager.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/13/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGServerManager.h"
#import "AFNetworking.h"
#import "SCLAlertView.h"
#import "NGViewControllerMapTable.h"
#import "NGAccessToken.h"
#import "NGMissionData.h"
#import "NGFullMessageDialog.h"
#import "NGMessageData.h"
#import <MRProgress/MRProgressOverlayView+AFNetworking.h>




@interface NGServerManager ()

@property (strong, nonatomic) AFHTTPRequestOperationManager *requestOperationManager;

@property (strong, nonatomic) NGAccessToken* accessToken;

@end

@implementation NGServerManager

+ (NGServerManager *) sharedManager {
    
    static NGServerManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        manager = [[NGServerManager alloc] init];
        
    });
    
    return manager;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.accessToken = [[NGAccessToken alloc] init];
    }
    return self;
}


- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}


#pragma mark - Registration in App

- (void) registration: (NSString *) email
            withLogin: (NSString *) login
          andPassword: (NSString *) password
            onSuccess: (void(^) (NSDictionary *dict)) success
            onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      email,        @"email",
                                      login,        @"login",
                                      password,     @"password",nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/registration.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              //self.accessToken.token = [responseObject objectForKey:@"token"];
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  
                  self.accessToken.token = [responseObject objectForKey:@"token"];
                  
                  NSString * errorString = @"error";
                  NSString *object = [responseObject objectForKey:@"object"];
                  
                  if ([object isEqualToString:errorString]) {
                      
                      NSLog(@"Erroreregnfjdgbnfdbnfzk");
                  }
                    else {
                        NSLog(@"Second button tapped");
                  }
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);

                  failure(error);
              }
          }];
}

#pragma mark - Login User for Facebook

- (void)    withLoginUserFacebook: (NSString *) loginUserFacebook
                        onSuccess: (void(^) (NSDictionary *dict)) success
                        onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      loginUserFacebook, @"token_fb", nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/login_fb.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  failure(error);
              }
          }];
    
}

#pragma mark -  LoginUser und password

- (void)    withLoginUser: (NSString *) loginUser
          andPasswordUser: (NSString *) passwordUser
            onSuccess: (void(^) (NSDictionary *dict)) success
            onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      loginUser,        @"login",
                                      passwordUser,     @"password",nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/login.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //self.accessToken.token = [responseObject objectForKey:@"user"];
              
//              NSLog(@"responseObject class: %@", [responseObject class]);
//              
          NSDictionary* userInfo = [responseObject objectForKey:@"user"];
              self.accessToken.token  = [userInfo objectForKey:@"token"];
              success(responseObject);
              
              if(success) {
                  
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  failure(error);
              }
          }];
     //[self.activityIndicatorView setAnimatingWithStateOfTask:task];
}


#pragma mark - Logout

- (void)    withClickLogout: (NSString *) click
                onSuccess: (void(^) (NSDictionary *dict)) success
                  onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                       self.accessToken.token,        @"token", nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/logout.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  failure(error);
              }
          }];
}

#pragma mark - Create Mission

- (void)    setTitle: (NSString *) title
       setTime_start: (NSString *) time_start
         setTime_end: (NSString *) time_end
      setPrice_start: (NSString *) price_start
        setPrice_end: (NSString *) price_end
      setDescription: (NSString *) descriptionOne
      setTitleRegion: (NSString *)titleRegion
    setAddressRegion: (NSString *)addressRegion
         setLatitude: (NSString *) latitude
        setLongitude: (NSString *) longitude
   setDescriptionTwo: (NSString *) descriptionTwo
           onSuccess: (void(^) (NSDictionary *dict)) success
           onFailure: (void(^) (NSError *error)) failure{
    
    NSDictionary *requestParametrs =
  
  @{
          @"title"          : title,
          @"time_start"     : time_start,
          @"time_end"       : time_end,
          @"price_start"    : price_start,
          @"price_end"      : price_end,
          @"description"    : descriptionOne,
          @"token"          :self.accessToken.token,
          @"place"          :
                                @{
                                    @"title"      :titleRegion,
                                    @"address"    :addressRegion,
                                    @"latitude"   :latitude,
                                    @"longitude"  :longitude,
                                    @"description":descriptionTwo,
                 
                                    }};
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/mission_create.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}



#pragma mark - Forget Password

- (void) addOldPass: (NSString *) passwordOld
            addNewPass: (NSString *) passwordNew
          addConfirm: (NSString *) confirm
            onSuccess: (void(^) (NSDictionary *dict)) success
            onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      passwordOld,        @"password_old",
                                      passwordNew,        @"password_new",
                                      confirm,            @"confirm",nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/password_change.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}


#pragma mark - Get Mission

- (void)
      addCreator: (NSString *) creator
     addProvider: (NSString *) provider
          addNum: (NSString *) num
      addPerpage: (NSString *) perpage
    addSort_flow: (NSString *) sort_flow
      addSort_by: (NSString *) sort_by
addUser_latitude: (NSString *) user_latitude
addUser_longitude:(NSString *) user_longitude
          addDis: (NSString *) dis
      addDis_cut: (NSString *) dis_cut
      addstatusM: (NSString *) statusM
          onSuccess: (void(^) (NSDictionary *dict)) success
          onFailure: (void(^) (NSError *error)) failure {

    NSDictionary *requestParametrs =
    
    @{
      @"token"            : self.accessToken.token,
      @"creator"          : creator,
      @"provider"         : provider,
      @"statusM"          : statusM,
      @"page"             :
          @{
              @"num"              :num,
              @"perpage"          :perpage,
                              },
      @"conditions"       :
          @{
              @"sort_flow"        :sort_flow,
              @"sort_by"          :sort_by,
              @"user_latitude"    :user_latitude,
              @"user_longitude"   :user_longitude,
              @"dis"              :dis,
              @"dis_cut"          :dis_cut,
            }};
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/mission_party.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", responseObject);


              NSArray* dictsArray = [responseObject objectForKey:@"missionParty"];
              NSMutableArray* objectsArray = [NSMutableArray array];
              
              for (NSDictionary* dict in dictsArray) {
                  
                  NGMissionData* mission = [[NGMissionData alloc] initWithServerResponse:dict];
                  [objectsArray addObject:mission];
              }

              if (success) {
                  success(responseObject);
             
                  
                  NSString* response = [responseObject objectForKey:@"text"];
                  //_accessToken.token  = [userInfo objectForKey:@"token"];
                  
                  SCLAlertView *alert = [[SCLAlertView alloc] init];
                  [alert showError:nil title:@"Ohh" subTitle:response closeButtonTitle:@"OK" duration:0.0f];
                }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}

#pragma mark - Upload photo

- (void)  addAlbom: (NSString *) albom
       addFilePath: (NSString *) filePath
onSuccess: (void(^) (NSDictionary *dict)) success
onFailure: (void(^) (NSError *error)) failure {
    
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      self.accessToken.token, @"token",
                                      albom,                  @"album",
                                  imageData,                  @"img",    nil];

    
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
//    NSData *imageData = UIImageJPEGRepresentation( filePath, 0.5);
//    //NSDictionary *parameters = @{@"username": self.username, @"password" : self.password};
//    
//    AFHTTPRequestOperation *op = [manager POST:@"http://pprapp.portaleb.com/api/alone/upload_photo.php" parameters:requestParametrs constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileData:imageData name:paramNameForImage fileName:@"photo.jpg" mimeType:@"image/jpeg"];
//    }
//        	success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
//    }];
//    [op start];
}

#pragma mark - Accept Send

- (void) addid_mission: (NSString *) id_mission
                addmsg: (NSString *) msg
              addprice: (NSString *) price
          onSuccess: (void(^) (NSDictionary *dict)) success
          onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      self.accessToken.token, @"token",
                                      id_mission,             @"id_mission",
                                      msg,                    @"msg",
                                      price,                  @"price",         nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/accept_send.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}

#pragma mark - Get Dialog

- (void) addNum: (NSString *) num
     addPerpage: (NSString *) perpage
      onSuccess: (void(^) (NSDictionary *dict)) success
      onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs =
    
    @{
      @"token"            : self.accessToken.token,
      @"page"             :
          @{
              @"num"              :num,
              @"perpage"          :perpage,
              }};

    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/msg_dialogs.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              
              NSArray* dictsArray = [responseObject objectForKey:@"messageParty"];
              NSMutableArray* objectsArray = [NSMutableArray array];
              
              for (NSDictionary* dict in dictsArray) {
                  
                  NGMessageData* message = [[NGMessageData alloc] initWithServerResponse:dict];
                  [objectsArray addObject:message];

              }
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}


#pragma mark - Profile Settings

- (void) addUser_name: (NSString *) user_name
      addUser_surname: (NSString *) user_surname
          addNickname: (NSString *) nickname
         addBirthdate: (NSString *) birthdate
             addEmail: (NSString *) email
             addId_fb: (NSString *) id_fb
            addStatus: (NSString *) status
              addRole: (NSString *) role
               addCar: (NSString *) car
           addAddress: (NSString *) address
             addPhone: (NSString *) phone
        addCreditCard: (NSString *) creditCard
           addBicycle: (NSString *) bicycle
              addMoto: (NSString *) moto
          addCarexist: (NSString *) carexist
               addVan: (NSString *) van
             addTruck: (NSString *) truck
               addBus: (NSString *) bus
              addShip: (NSString *) ship
          addLicensep: (NSString *) licensep
          addLatitude: (NSString *) latitude
         addLongitude: (NSString *) longitude
             addOther: (NSString *) other
           addCountry: (NSString *) country
              addCity: (NSString *) city

   onSuccess: (void(^) (NSDictionary *dict)) success
   onFailure: (void(^) (NSError *error)) failure {



    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      self.accessToken.token,       @"token",
                                      user_name,                    @"user_name",
                                      user_surname,                 @"user_surname",
                                      nickname,                     @"nickname",
                                      birthdate,                    @"birthdate",
                                      email,                        @"email",
                                      id_fb,                        @"id_fb",
                                      status,                       @"status",
                                      role,                         @"role",
                                      car,                          @"car",
                                      address,                      @"address",
                                      phone,                        @"phone",
                                      creditCard,                   @"creditCard",
                                      bicycle,                      @"bicycle",
                                      moto,                         @"moto",
                                      carexist,                     @"carexist",
                                      van,                          @"van",
                                      truck,                        @"truck",
                                      bus,                          @"bus",
                                      ship,                         @"ship",
                                      licensep,                     @"licensep",
                                      latitude,                     @"latitude",
                                      longitude,                    @"longitude",
                                      other,                        @"other",
                                      country,                      @"country",
                                      city,                         @"city",
                                      nil];
    
    
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    
    NSString * URL = @"http://pprapp.portaleb.com/api/alone/profile_settings.php";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    [manager setResponseSerializer:responseSerializer];

    [manager POST:URL
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"operation: %@", operation);
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}

#pragma mark - Profile Show

- (void) addId_user: (NSString *) id_user
      onSuccess: (void(^) (NSDictionary *dict)) success
      onFailure: (void(^) (NSError *error)) failure {
    
    
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      self.accessToken.token,       @"token",
                                      id_user,                      @"id_user", nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/profile_show.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}

#pragma mark - Send Message

- (void)         addto:  (NSString *) to
                addmsg:  (NSString *) msg
              addId_msg: (NSString *) id_msg
             onSuccess: (void(^) (NSDictionary *dict)) success
             onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      self.accessToken.token, @"token",
                                      to,             @"to",
                                      msg,            @"msg",
                                      id_msg,         @"id_msg",         nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/msg_send.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}


#pragma mark - Show accepted user

- (void) addid_mission:  (NSString *) id_mission
                addnum:  (NSString *) num
             addperpage: (NSString *) perpage
             onSuccess: (void(^) (NSDictionary *dict)) success
             onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs =
    
    @{
      @"id_mission"       : id_mission,
      @"token"            : self.accessToken.token,
      @"page"             :
          @{
              @"num"              :num,
              @"perpage"          :perpage,
              }};
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/accept_showM.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}

#pragma mark - Get Provider

- (void) addDistancion: (NSString *) distancion
             onSuccess: (void(^) (NSDictionary *dict)) success
             onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      self.accessToken.token, @"token",
                                      distancion,             @"dis",
                                      nil];
    
    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/profile/getProvidersFixedLocation.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}

#pragma mark - Get Dialog

- (void) addidDialog: (NSString *) id_dialog
        addIdMessage: (NSString *) id_msg
             addPage: (NSString *) page
             onSuccess: (void(^) (NSDictionary *dict)) success
             onFailure: (void(^) (NSError *error)) failure {
    
    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
                                      self.accessToken.token, @"token",
                                      id_dialog,              @"id_dialog",
                                      id_msg,                 @"id_msg",
                                      page,                   @"page",
                                      nil];

    NSLog(@"requestParametrs: %@", requestParametrs);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:@"http://pprapp.portaleb.com/api/alone/msg_dialog_full.php"
     
       parameters:requestParametrs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSArray* dictsArray = [responseObject objectForKey:@"missionParty"];
              NSMutableArray* objectsArray = [NSMutableArray array];
              
              for (NSDictionary* dict in dictsArray) {
                  
                  NGFullMessageDialog* dialogFull = [[NGFullMessageDialog alloc] initWithServerResponse:dict];
                  [objectsArray addObject:dialogFull];
              }
              
              NSLog(@"responseObject class: %@", [responseObject class]);
              
              if(success) {
                  
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              if(failure) {
                  
                  NSLog(@"ERROR: %@", error);
                  
                  failure(error);
              }
          }];
}


@end