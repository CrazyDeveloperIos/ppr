//
//  NGСustomUserAcceptVC.m
//  Uni Bit
//
//  Created by Naz on 6/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGCustomUserAcceptVC.h"
#import "NGMyMissionViewController.h"
#import "NGServerManager.h"
#import "AFNetworking.h"
#import "NGAccessToken.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"
#import "customUserAcceptTableViewCell.h"
#import "NGAcceptedUserData.h"
#import "NGDefultCurrentProfileInfo.h"
#import "NGViewControllerMapTable.h"


@interface NGCustomUserAcceptVC ()

@property (strong, nonatomic) NSString *textResponce;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *idMission;
@property (strong, nonatomic) NSArray* messageParty;
@property (strong, nonatomic) NSMutableArray* arrayUser;
@property (strong, nonatomic) NSDictionary* userParty;
@property (nonatomic, strong) NSDictionary *allUsers;
@property (nonatomic ,strong) UIRefreshControl *refreshView;
@property (strong, nonatomic) NSString *id_user;
@property (strong, nonatomic) NGAcceptedUserData *applications;

@end

@implementation NGCustomUserAcceptVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:@"acceptedMissionId" object:nil];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;
    self.refreshView = [[UIRefreshControl alloc] init];
    
    [self.refreshView addTarget:self action:@selector(getAcceptedUser) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshView];
    self.okButton.layer.cornerRadius = CGRectGetHeight(self.okButton.bounds)/2;
    self.okButton.layer.masksToBounds = YES;
    self.arrayUser = [NSMutableArray new];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)okButton:(id)sender {

    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)showProfileAcceptedUser:(id)sender {
    
    NGDefultCurrentProfileInfo *infoUserVC =
    [self.storyboard instantiateViewControllerWithIdentifier:@"infoUserVC"];
    //[self presentViewController:messageVC animated:YES completion:nil];
    [self showViewController:infoUserVC sender:nil];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.applications.provider forKey:@"id"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userAccepted"
                                                        object:userInfo
                                                      userInfo:userInfo];
    

}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    if (notification.object != NULL)
    {
        self.idMission = notification.object;
    }
    
    [self getAcceptedUser];
}

- (void) getAcceptedUser {

    
    [[NGServerManager sharedManager]
     addid_mission:self.idMission
     addnum:       @"1"
     addperpage:   @"4"
     
     onSuccess:^(NSDictionary *dict) {
         [self.arrayUser removeAllObjects];
         
         
         NSArray *application = dict[@"application"];
         for (NSDictionary *applications in application){
             NGAcceptedUserData *missionData = [[NGAcceptedUserData alloc] initWithServerResponse:applications];
             [self.arrayUser addObject:missionData];
         }
         self.allUsers = dict[@"user"];
         [self.tableView reloadData];
         [self.refreshView endRefreshing];
    }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         [self.refreshView endRefreshing];
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"" subTitle:@"Not internet connection" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];
    
    
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayUser.count;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (self.tableView == tableView){
        self.applications = self.arrayUser[indexPath.row];
        static NSString *simpleTableIdentifier = @"Cell";
        
        customUserAcceptTableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if(!cell){
            cell = [[customUserAcceptTableViewCell alloc] initWithStyle:
                    UITableViewCellStyleDefault      reuseIdentifier:simpleTableIdentifier];
        }
        
        NSString * server = @"http://agent1.kievregion.net";
        NSString * photo = self.allUsers[self.applications.provider][@"photo"][@"url"];
        NSString * avatar = [NSString stringWithFormat:@"%@%@", server, photo];
        NSURL* img;
        if (avatar) {
            img = [NSURL URLWithString:avatar];
        }
        [cell.imageProfile setImageWithURL:img placeholderImage:[UIImage imageNamed:@"userimg"]];
        NSString* name = self.allUsers[self.applications.provider][@"nickname"];
        //self.id_user = self.allUsers[self.applications.provider][@"id_user"];
        cell.nicknameLabel.text = name;
        cell.priceLabel.text = self.applications.msg;
        //cell.userImage.image;
        
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (IBAction)startMissionButton:(id)sender {
    
    NSLog(@"Hello");
    
}

@end
