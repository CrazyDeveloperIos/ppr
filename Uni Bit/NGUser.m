//
//  NGUser.m
//  Uni Bit
//
//  Created by Naz on 6/29/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGUser.h"

@implementation NGUser
- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        self.id_mission = [responseObject objectForKey:@"id_mission"];
        self.msg = [responseObject objectForKey:@"msg"];
        self.price = [responseObject objectForKey:@"price"];
        self.provider = [responseObject objectForKey:@"provider"];
        self.time = [responseObject objectForKey:@"time"];
        
        self.id_user = [responseObject objectForKey:@"id_user"];
        self.name = [responseObject objectForKey:@"name"];
        self.nickname = [responseObject objectForKey:@"nickname"];
        self.photoID = [responseObject objectForKey:@"photoID"];
        self.photoURL = [responseObject objectForKey:@"photoURL"];
        self.status = [responseObject objectForKey:@"status"];
        self.role = [responseObject objectForKey:@"role"];
        
    }
    return self;
}

-(NSString *)description   {
    return [NSString stringWithFormat:@"from %@, id_dialog %@, id_msg %@, text %@, time_create %@, to%@", self.id_mission, self.msg, self.price ,self.provider, self.time, self.nickname];
}

@end
