//
//  RegisterViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/10/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGRegisterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)registerButton:(id)sender;

- (IBAction)policiesButton:(id)sender;
@end
