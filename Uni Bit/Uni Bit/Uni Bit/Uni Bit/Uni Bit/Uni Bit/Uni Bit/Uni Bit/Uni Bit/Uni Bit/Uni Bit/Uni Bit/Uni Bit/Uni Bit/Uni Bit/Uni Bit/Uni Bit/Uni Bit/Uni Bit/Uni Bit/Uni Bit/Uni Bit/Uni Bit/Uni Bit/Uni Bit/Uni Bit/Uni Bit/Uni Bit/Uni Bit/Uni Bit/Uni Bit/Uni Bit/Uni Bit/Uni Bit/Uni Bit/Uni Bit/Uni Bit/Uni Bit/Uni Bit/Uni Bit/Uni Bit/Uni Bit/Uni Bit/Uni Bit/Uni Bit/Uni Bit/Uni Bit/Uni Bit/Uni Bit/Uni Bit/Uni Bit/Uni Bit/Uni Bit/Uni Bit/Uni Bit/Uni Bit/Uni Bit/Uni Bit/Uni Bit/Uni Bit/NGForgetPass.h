//
//  NGForgetPass.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGForgetPass : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *currentPass;

@property (weak, nonatomic) IBOutlet UITextField *novuyPassword;

@end
