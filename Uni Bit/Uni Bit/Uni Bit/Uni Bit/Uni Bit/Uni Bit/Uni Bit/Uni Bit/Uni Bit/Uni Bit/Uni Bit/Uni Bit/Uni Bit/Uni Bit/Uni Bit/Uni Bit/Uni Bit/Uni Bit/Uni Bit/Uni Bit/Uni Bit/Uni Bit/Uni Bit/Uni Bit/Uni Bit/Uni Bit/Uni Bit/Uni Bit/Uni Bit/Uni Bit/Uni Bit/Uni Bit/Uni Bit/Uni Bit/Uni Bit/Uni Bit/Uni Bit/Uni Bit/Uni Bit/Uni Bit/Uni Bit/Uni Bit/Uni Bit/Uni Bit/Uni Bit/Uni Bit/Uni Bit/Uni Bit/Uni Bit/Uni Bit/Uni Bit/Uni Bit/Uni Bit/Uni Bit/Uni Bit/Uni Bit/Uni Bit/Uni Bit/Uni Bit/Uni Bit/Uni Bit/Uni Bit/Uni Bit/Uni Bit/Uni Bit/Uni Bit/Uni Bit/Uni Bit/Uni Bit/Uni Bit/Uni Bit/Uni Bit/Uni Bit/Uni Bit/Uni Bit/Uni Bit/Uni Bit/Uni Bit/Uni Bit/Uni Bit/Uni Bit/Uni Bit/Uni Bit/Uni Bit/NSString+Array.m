//
//  NSString+Array.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NSString+Array.h"

@implementation NSString (Array)
+ (NSString *)stringFromArray:(NSArray *) array{
    NSString *result = [NSString new];
        for (NSString *string in array){
            result =  [result stringByAppendingString:string];
            result = [result stringByAppendingString:@"\n"];
        }
        return result;
    }
@end
