//
//  NGForgetPass.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGForgetPass.h"
#import "NGServerManager.h"
#import "SCLAlertView.h"
#import "NGViewControllerMapTable.h"

@implementation NGForgetPass

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    // Do any additional setup after loading the view.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (IBAction)forgetPassButton:(id)sender {
    
    if (self.currentPass.text.length != 0 || self.novuyPassword.text.length != 0) {
        
        
        [[NGServerManager sharedManager]
         
         addOldPass:self.currentPass.text
         addNewPass:self.novuyPassword.text
         addConfirm:@"okay"
         
         onSuccess:^(NSDictionary *dict) {
             
             SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
             
             [alert addButton:@"New Password save" actionBlock:^(void) {
                 
                 NGViewControllerMapTable *centralNavigationController =
                 [self.storyboard instantiateViewControllerWithIdentifier:@"centralNavigationController"];
                 [self.navigationController pushViewController:centralNavigationController animated:YES];
                 NSLog(@"Second button tapped");
             }];
             
             alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/right_answer.mp3", [[NSBundle mainBundle] resourcePath]]];
             
             [alert showSuccess:@"" subTitle:@"" closeButtonTitle:@"Done" duration:0.0f];
             
         }
         onFailure:^(NSError *error) {
             
             NSLog(@"ERROR: %@", error);
             
             SCLAlertView *alert = [[SCLAlertView alloc] init];
             [alert showError:self title:@"Ohh" subTitle:@"You add bad login or password" closeButtonTitle:@"OK" duration:0.0f]; // Error
             
         }];
        
        
    }
    else {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Oh!" subTitle:@"You need add login and password" closeButtonTitle:@"OK" duration:0.0f]; // Error
    }
    
    

}

@end
