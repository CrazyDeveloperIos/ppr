//
//  RegisterViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/10/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGRegisterViewController.h"
#import "NGServerManager.h"
#import "NGViewControllerMapTable.h"
#import "SCLAlertView.h"

@interface NGRegisterViewController ()<UINavigationBarDelegate , UINavigationControllerDelegate>

@end

@implementation NGRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - registerButton

- (IBAction)registerButton:(id)sender {

    [[NGServerManager sharedManager] registration:self.emailTextField.text
                                        withLogin:self.loginTextField.text
                                      andPassword:self.passwordTextField.text
                                        onSuccess:^(NSDictionary *dict) {
                                            
                                            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                                            
                                            [alert addButton:@"Succesed" actionBlock:^(void) {
                                                NGViewControllerMapTable *ngVCMapTable =
                                                [self.storyboard instantiateViewControllerWithIdentifier:@"ngVCMapTable"];
                                                [self.navigationController pushViewController:ngVCMapTable animated:YES];
                                                NSLog(@"Second button tapped");
                                            }];
                                            
                                            alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/right_answer.mp3", [[NSBundle mainBundle] resourcePath]]];
                                            
                                            [alert showSuccess:@"" subTitle:@"" closeButtonTitle:@"Done" duration:1.5f];
                                        
                                        }
                                        onFailure:^(NSError *error) {
                                            
                                            NSLog(@"ERROR: %@", error);
                                            
                                            SCLAlertView *alert = [[SCLAlertView alloc] init];
                                            [alert showError:self title:@"Hello Error" subTitle:@"This is a more descriptive error text." closeButtonTitle:@"OK" duration:0.0f]; // Error
                                            
                                        }];
    

}

#pragma mark - successAlert

-(void) successAlert:(id)sender {
    NGViewControllerMapTable *ngVCMapTable =
    [self.storyboard instantiateViewControllerWithIdentifier:@"ngVCMapTable"];
    [self showViewController:ngVCMapTable sender:ngVCMapTable];
    
}

#pragma mark - policiesButton

- (IBAction)policiesButton:(id)sender {
}

#pragma mark - doneAction

- (IBAction)doneAction:(UIBarButtonItem *)sender {
    
    
    [UIView animateWithDuration:0.8
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         [UIView setAnimationTransition:
                          UIViewAnimationTransitionFlipFromRight
                                                forView:self.navigationController.view cache:NO];
                         
                         [self.navigationController popViewControllerAnimated:YES];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}
@end
