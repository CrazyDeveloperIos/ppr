//
//  NGAcceptMissionsViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/16/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGViewControllerMapTable.h"
#import "NGAcceptMissionsViewController.h"


@interface NGAcceptMissionsViewController : UIViewController<GMSMapViewDelegate>

@end
