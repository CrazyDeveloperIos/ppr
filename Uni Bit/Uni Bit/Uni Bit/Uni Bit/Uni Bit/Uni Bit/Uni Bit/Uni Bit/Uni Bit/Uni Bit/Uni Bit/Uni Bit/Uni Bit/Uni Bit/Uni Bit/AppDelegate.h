//
//  AppDelegate.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/5/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

