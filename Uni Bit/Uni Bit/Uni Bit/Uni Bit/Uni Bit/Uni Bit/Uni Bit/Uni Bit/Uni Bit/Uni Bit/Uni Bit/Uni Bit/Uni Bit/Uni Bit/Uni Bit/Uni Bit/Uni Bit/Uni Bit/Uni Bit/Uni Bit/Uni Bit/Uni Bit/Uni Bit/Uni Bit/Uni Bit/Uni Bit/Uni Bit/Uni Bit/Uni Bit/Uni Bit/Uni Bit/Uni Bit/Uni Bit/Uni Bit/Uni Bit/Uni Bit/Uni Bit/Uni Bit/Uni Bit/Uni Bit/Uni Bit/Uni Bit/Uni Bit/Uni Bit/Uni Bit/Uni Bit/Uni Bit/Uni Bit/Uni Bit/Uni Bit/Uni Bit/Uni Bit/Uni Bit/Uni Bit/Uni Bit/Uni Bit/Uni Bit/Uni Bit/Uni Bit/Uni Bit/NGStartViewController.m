//
//  ViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/5/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGStartViewController.h"
#import "NGViewControllerMapTable.h"
#import "NGServerManager.h"
#import "SCLAlertView.h"

@interface NGStartViewController ()<FBSDKLoginTooltipViewDelegate, FBSDKLoginButtonDelegate>

@end

@implementation NGStartViewController

- (void)viewDidLoad {
    [self getStatusUserInApp];
    [super viewDidLoad];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - connectionButton

- (IBAction)connectionButton:(id)sender { 
    if (self.emalTextField.text.length != 0 || self.passwordTextField.text.length != 0) {
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        // saving an NSString
        [prefs setObject:self.emalTextField.text forKey:@"userName"];
        [prefs setObject:self.passwordTextField.text forKey:@"password"];
        
        [prefs synchronize];
        
        NSString *userName = [prefs objectForKey:@"userName"];
        NSString *password = [prefs objectForKey:@"password"];
        
        [[NGServerManager sharedManager]
         withLoginUser:userName
         andPasswordUser:password
         onSuccess:^(NSDictionary *dict) {
             
             NGViewControllerMapTable *centralNavigationController =
             [self.storyboard instantiateViewControllerWithIdentifier:@"centralNavigationController"];
             [self.navigationController pushViewController:centralNavigationController animated:YES];
             NSLog(@"Second button tapped");
             
             
         }
         onFailure:^(NSError *error) {
             
             NSLog(@"ERROR: %@", error);
             
             SCLAlertView *alert = [[SCLAlertView alloc] init];
             [alert showError:self title:@"Ohh" subTitle:@"You add bad login or password" closeButtonTitle:@"OK" duration:0.0f]; // Error
             
         }];
        
        
    }
    else {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Oh!" subTitle:@"You need add login and password" closeButtonTitle:@"OK" duration:0.0f]; // Error
    }
    
    
    
    ///                                                        AVATAR !!!!!!!
    
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http:xxx.me.com/me.json"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:100.0];
    //
    //
    //    NSData *imageData = UIImagePNGRepresentation ([UIImage imageWithContentsOfFile: @"ur PNG image path"]);
    //    [Base64 initialize];
    //    NSString *imageString = [Base64 encode:imageData];
    //
    //    NSArray *keys = [NSArray arrayWithObjects:@"image_id",@"image_name","image",nil];
    //    NSArray *objects = [NSArray arrayWithObjects:@"22",@"myImageName.png",imageString,nil];
    //
    //    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    //
    //    NSError *error;
    //    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:kNilOptions error:&error];
    //
    //    [request setHTTPMethod:@"POST"];
    //    [request setValue:[NSString stringWithFormat:@"%d",[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request setHTTPBody:jsonData];
    //
    //    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}


- (void) getStatusUserInApp {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [prefs objectForKey:@"userName"];
    NSString *password = [prefs objectForKey:@"password"];
    
    [[NGServerManager sharedManager]
     withLoginUser:userName
     andPasswordUser:password
     onSuccess:^(NSDictionary *dict) {
         
         NGViewControllerMapTable *centralNavigationController =
         [self.storyboard instantiateViewControllerWithIdentifier:@"centralNavigationController"];
         [self.navigationController pushViewController:centralNavigationController animated:YES];
         NSLog(@"Second button tapped");
         
         
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
     }];
    
}

#pragma mark - loginButtonFacebook

- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{
    
    if ([FBSDKAccessToken currentAccessToken]) {
        NSString *fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"  parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 SCLAlertView *alert = [[SCLAlertView alloc] init];
                 
                 [alert showSuccess:self title:([result objectForKey:@"name"]) subTitle:@"Login and Password send to you email" closeButtonTitle:@"Ok" duration:8.0f];
                 
                 NSLog(@"email is %@", [result objectForKey:@"email"]);
//
                 [[NGServerManager sharedManager]
                  
                  withLoginUserFacebook :fbAccessToken
                  
                  onSuccess:^(NSDictionary *dict) {
                      
                      SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                      
                      
                      [alert showSuccess:([result objectForKey:@"name"]) subTitle:@"Login and Password send to you email" closeButtonTitle:@"" duration:4.0f];
                      
                  }
                  onFailure:^(NSError *error) {
                      
                      NSLog(@"ERROR: %@", error);
                      
                      //                      SCLAlertView *alert = [[SCLAlertView alloc] init];
                      //                      [alert showError:self title:@"Hello Error" subTitle:@"This is a more descriptive error text." closeButtonTitle:@"OK" duration:0.0f]; // Error
                      
                  }];
                 
             }
             else{
                 NSLog(@"%@", [error localizedDescription]);
             }
         }];
    }
    else {
        NSLog(@"Error");
    }
    
}

#pragma mark - loginButtonDidLogOutFacebook

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
    [[NGServerManager sharedManager] withClickLogout:nil
     
                                           onSuccess:^(NSDictionary *dict) {
                                               
                                           }
                                           onFailure:^(NSError *error) {
                                               
                                               //                                            NSLog(@"ERROR: %@", error);
                                               //
                                               //                                            SCLAlertView *alert = [[SCLAlertView alloc] init];
                                               //                                            [alert showError:self title:@"Hello Error" subTitle:@"This is a more descriptive error text." closeButtonTitle:@"OK" duration:0.0f]; // Error
                                               
                                           }];
    
    
}

#pragma mark - prepareForSegue

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if ([segue.identifier isEqualToString:@"register"]) {
//        [UIView animateWithDuration:0.8
//                              delay:0
//                            options:UIViewAnimationOptionTransitionFlipFromBottom
//                         animations:^{
//                             [UIView setAnimationTransition:
//                              UIViewAnimationTransitionFlipFromRight
//                                                    forView:self.navigationController.view cache:NO];
//
//                         }
//                         completion:^(BOOL finished){
//
//                         }];
//    }
//}
@end