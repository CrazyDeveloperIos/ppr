//
//  MZUserTrackingInfoViewController.m
//  Мій Житомир
//
//  Created by Vladimir Matukh on 4/4/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import "NGUserTrackingInfoView.h"
#import "NGMissionData.h"

@interface NGUserTrackingInfoView () 
@property (nonatomic, strong) NGMissionData *responseObject;
@property (nonatomic, strong) NGViewControllerMapTable* missionsArray;
@end

@implementation NGUserTrackingInfoView


@end
