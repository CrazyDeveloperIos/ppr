//
//  NGAccessToken.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGAccessToken.h"

@implementation NGAccessToken
-(instancetype)init{
    self = [super init];
    if (self){
        
    }
    return self;
}
-(void)setToken:(NSUserDefaults *)token{
    _token = token;
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
}
@end
