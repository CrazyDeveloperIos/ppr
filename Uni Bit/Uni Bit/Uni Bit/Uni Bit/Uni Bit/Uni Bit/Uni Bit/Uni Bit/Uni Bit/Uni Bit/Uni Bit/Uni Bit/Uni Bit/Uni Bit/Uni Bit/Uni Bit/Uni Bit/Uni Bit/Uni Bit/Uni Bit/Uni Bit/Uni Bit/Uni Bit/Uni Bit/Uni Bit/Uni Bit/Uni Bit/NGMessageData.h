//
//  NGMessageData.h
//  Uni Bit
//
//  Created by Naz on 6/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGServetObject.h"

@interface NGMessageData : NGServetObject
@property (strong, nonatomic) NSString* id_user;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* nickname;
@property (strong, nonatomic) NSString* photoID;
@property (strong, nonatomic) NSString* photoURL;
@property (strong, nonatomic) NSString* status;
@property (strong, nonatomic) NSString* from;
@property (strong, nonatomic) NSString* id_dialog;
@property (strong, nonatomic) NSString* id_msg;
@property (strong, nonatomic) NSString* text;
@property (strong, nonatomic) NSString* time_create;
@property (strong, nonatomic) NSString* to;
@end
