//
//  NGBestMissionViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface NGBestMissionViewController : UIViewController

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;


@end
