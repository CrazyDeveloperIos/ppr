//
//  NGMissionData.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/29/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGServetObject.h"

@interface NGMissionData : NGServetObject

@property (strong, nonatomic) NSString* creator;
@property (strong, nonatomic) NSString* descriptionOne;
@property (nonatomic) NSString* distanse;
@property (strong, nonatomic) NSString* id_mission;
@property (strong, nonatomic) NSString* addressPlace;
@property (strong, nonatomic) NSString* descriptionPlace;
@property (strong, nonatomic) NSString* id_Place;
@property (strong, nonatomic) NSNumber* latitude;
@property (strong, nonatomic) NSNumber* longitude;
@property (strong, nonatomic) NSString* titlePlace;
@property (strong, nonatomic) NSString* reference_num;
@property (strong, nonatomic) NSString* status;
@property (nonatomic) NSString* time_end;
@property (nonatomic) NSString* time_start;
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* type;
@property (strong, nonatomic) NSString* id_User;
@property (strong, nonatomic) NSString* name_User;
@property (strong, nonatomic) NSString* nickname_User;
@property (strong, nonatomic) NSString* photoURl;
@property (strong, nonatomic) NSString* surname_User;
@property (strong, nonatomic) NSString* status_User;
@end
