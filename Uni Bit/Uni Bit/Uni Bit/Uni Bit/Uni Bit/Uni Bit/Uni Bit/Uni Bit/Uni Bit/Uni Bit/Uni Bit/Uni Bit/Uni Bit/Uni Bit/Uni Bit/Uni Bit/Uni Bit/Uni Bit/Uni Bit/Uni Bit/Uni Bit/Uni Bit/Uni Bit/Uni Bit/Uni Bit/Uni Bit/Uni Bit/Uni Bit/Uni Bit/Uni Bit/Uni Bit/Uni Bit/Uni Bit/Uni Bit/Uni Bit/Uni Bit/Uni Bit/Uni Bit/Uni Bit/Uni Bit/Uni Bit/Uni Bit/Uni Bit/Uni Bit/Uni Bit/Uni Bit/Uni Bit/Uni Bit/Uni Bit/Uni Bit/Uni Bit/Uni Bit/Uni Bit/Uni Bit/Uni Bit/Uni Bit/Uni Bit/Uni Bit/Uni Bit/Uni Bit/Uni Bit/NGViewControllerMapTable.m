//
//  NGViewControllerMapTable.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/6/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGViewControllerMapTable.h"
#import "CustomInfoWindow.h"
#import "NGCustomTableViewCell.h"
#import "NGUserTrackingInfoView.h"
#import "AcceptMissionView.h"
#import "NGServerManager.h"
#import <QuartzCore/QuartzCore.h>
#import "SCLAlertView.h"
#import "NGServetObject.h"
#import "UIImageView+AFNetworking.h"
#import "NGDefultCurrentProfileInfo.h"



@interface NGViewControllerMapTable ()<CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic) CLLocationCoordinate2D userCoordinate;
@property (weak, nonatomic) IBOutlet UITableView *linksTableView;
@property (nonatomic, strong) NSIndexPath *selectedCell;
@property (nonatomic, strong) NGUserTrackingInfoView *infoView;
@property (nonatomic, strong) AcceptMissionView *acceptMissionsView;
@property (nonatomic, strong) NSDictionary *allUsers;
@property (nonatomic, strong) NSDictionary *infoUserDic;
@property (nonatomic, strong) NSMutableArray *missionsArray;
@property (nonatomic, strong) NSString *textResponce;
@property (nonatomic, strong) NGMissionData* myMission;
@property (nonatomic ,strong) UIRefreshControl *refreshView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *userInfoVisualEffects;
@property (nonatomic ,strong) NSString *sort;
@property (nonatomic ,strong) NSString *idSelectedMission;
@property (nonatomic ,strong) NSString *userAvatar;
@property (nonatomic ,strong) NSString *IdUser;
@property (weak, nonatomic) IBOutlet UILabel *nicknameUserLabel;
@property (weak, nonatomic) IBOutlet UILabel *roleUserLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageAvatar;
@property (nonatomic ,strong) NSURL* imageURL;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birtDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UITextField *textMessageInUserInfo;
@property (weak, nonatomic) IBOutlet UIButton *sedmMessageInUserInfo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerConstrains;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heighVisualEffectConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstrainToVisualEffects;


@end

@implementation NGViewControllerMapTable



- (void)viewDidLoad {
    //CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    self.footerConstrains.constant = height + 700;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [self.locationManager startUpdatingLocation];
    //self.userAvatar = @"http://agent1.kievregion.net";
    self.sort = @"distance";
    [self refresh];
    self.missionsArray = [NSMutableArray new];
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = kGMSTypeHybrid;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;
    self.userCoordinate = self.mapView.myLocation.coordinate;
    
    
    self.userInfoVisualEffects.layer.cornerRadius = 5;
    self.userInfoVisualEffects.layer.masksToBounds = YES;
    
    self.userImageAvatar.layer.cornerRadius = CGRectGetHeight(self.userImageAvatar.bounds)/2;
    self.userImageAvatar.layer.masksToBounds = YES;
    self.okButton.layer.cornerRadius = CGRectGetHeight(self.okButton.bounds)/2;
    self.okButton.layer.masksToBounds = YES;
    
    
     self.infoView =(NGUserTrackingInfoView *) [[self.storyboard instantiateViewControllerWithIdentifier:@"inforamtionVC"] view];
    [self.infoView setFrame:CGRectMake(self.mapView.center.x - 125, CGRectGetHeight(self.mapView.frame), 250, 10)];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideAction:)];
    [tap setNumberOfTapsRequired:1];
    [self.infoView addGestureRecognizer:tap];
    UIButton *buttonClosed = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonClosed addTarget:self
                     action:@selector(hideAction:)
           forControlEvents:UIControlEventTouchDown];
    
    UIImage *buttonImage = [UIImage imageNamed:@"exit-closed.png"];
    [buttonClosed setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [buttonClosed setFrame:CGRectMake(250, -10, 30, 30)];
    [self.infoView addSubview:buttonClosed];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(openMoreInfo:)
     forControlEvents:UIControlEventTouchDown];
    UIImage *buttonAcept = [UIImage imageNamed:nil];
    [button setBackgroundImage:buttonAcept forState:UIControlStateNormal];
    [button setFrame:CGRectMake(100, 150, 70, 25)];
    [self.infoView addSubview:button];
    
    UIButton *buttonUserInfo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonUserInfo addTarget:self
               action:@selector(showInfoOrUser:)
     forControlEvents:UIControlEventTouchDown];
    UIImage *buttonAept = [UIImage imageNamed:nil];
    [buttonUserInfo setBackgroundImage:buttonAept forState:UIControlStateNormal];
    [buttonUserInfo setFrame:CGRectMake(10, 15, 70, 25)];
    [self.infoView addSubview:buttonUserInfo];
    
    
    self.refreshView = [[UIRefreshControl alloc] init];
    
    [self.refreshView addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshView];
    [self refreshView];
    
    
    
}

- (IBAction)filter:(id)sender {
    
    switch (((UISegmentedControl *)sender).selectedSegmentIndex) {
        case 0:
            self.sort =@"distance";
            [self refresh];
            [self.tableView reloadData];
            break;
            
        case 1:
            self.sort =@"date";
            [self refresh];
            [self.tableView reloadData];
            break;
        default:
            break;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void) refreshPins{
    for (NGMissionData *mission in self.missionsArray){
        GMSMarker *marker1 = [[GMSMarker alloc] init];
        marker1.position = CLLocationCoordinate2DMake([mission.latitude doubleValue], [mission.longitude doubleValue]);
        marker1.appearAnimation = kGMSMarkerAnimationPop;
        marker1.title = mission.title;
        marker1.icon = [UIImage imageNamed:@"pin12"];
        marker1.map = self.mapView;
        
    }
}

-(void) refresh {
    
    NSString *latitude = [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f",self.locationManager.location.coordinate.longitude];

    [[NGServerManager sharedManager]
     
     addCreator:@"0"
     addProvider:@"0"
     addNum:@"1"
     addPerpage:@"300"
     addSort_flow:@"0"
     addSort_by:self.sort
     addUser_latitude: latitude
     addUser_longitude:longitude
     addDis:@"30000"
     addDis_cut:@"0"
     addstatusM:@"n"
     
     
     onSuccess:^(NSDictionary *dict) {
         [self.missionsArray removeAllObjects];
         NSArray *missions = dict[@"missionParty"];
         for (NSDictionary *mission in missions){
             NGMissionData *missionData = [[NGMissionData alloc] initWithServerResponse:mission];
             [self.missionsArray addObject:missionData];
         }
         self.allUsers = dict[@"usersShort"];
          [self.tableView reloadData];
         [self refreshPins];

         [self.refreshView endRefreshing];
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
           [self.refreshView endRefreshing];
         
                  SCLAlertView *alert = [[SCLAlertView alloc] init];
                  [alert showError:self title:@"" subTitle:@"Not internet connection" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];

   
}

- (IBAction)sendComment:(id)sender {
         //self.textMessageInUserInfo
    
    [[NGServerManager sharedManager]
     
     addto:@"0"
     addmsg:self.textMessageInUserInfo.text
     addId_msg:@"1"
     
     
     onSuccess:^(NSDictionary *dict) {
         [self.missionsArray removeAllObjects];
         NSArray *missions = dict[@"missionParty"];
         for (NSDictionary *mission in missions){
             NGMissionData *missionData = [[NGMissionData alloc] initWithServerResponse:mission];
             [self.missionsArray addObject:missionData];
         }
         self.allUsers = dict[@"usersShort"];
         [self.tableView reloadData];
         [self refreshPins];
         
         [self.refreshView endRefreshing];
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         [self.refreshView endRefreshing];
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"" subTitle:@"Not internet connection" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];
    
    

}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.acceptMissionsView = (AcceptMissionView *)[[self.storyboard instantiateViewControllerWithIdentifier:@"AcceptViewController"] view];
    self.acceptMissionsView.layer.cornerRadius = 5;
    self.acceptMissionsView.layer.masksToBounds = YES;
    [self.acceptMissionsView setFrame:CGRectMake(70,-300, 0, 0)];
    UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideAction:)];
    [taps setNumberOfTapsRequired:1];
    [self.acceptMissionsView addGestureRecognizer:taps];
    [self.acceptMissionsView.closeButton addTarget:self action:@selector(hideAcceptView:) forControlEvents:UIControlEventTouchDown];
    [self.acceptMissionsView.sendButton addTarget:self action:@selector(acceptMission:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.acceptMissionsView];
    
}

- (void) acceptMission: (id) sender {
    [[NGServerManager sharedManager]
     
     addid_mission:self.idSelectedMission
            addmsg:self.acceptMissionsView.commentTextView.text
          addprice:self.acceptMissionsView.commentTextField.text
     
     onSuccess:^(NSDictionary *dict) {
         
         self.textResponce = dict[@"text"];
         self.textResponce = dict[@"action"];
         self.acceptMissionsView.commentTextField.text = @"";
         self.acceptMissionsView.commentTextView.text = @"";
         
         SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
         
         [alert showSuccess:@"" subTitle:self.textResponce  closeButtonTitle:@"Done" duration:3.0f];
         
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"Ohh" subTitle:@"Bab data" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];
    

    
    
    
    
    [self hideAcceptView: (id) sender];
    
    
    
}
#pragma mark - Map View

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = self.mapView.myLocation.coordinate;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.title = @"Hello World";
    marker.icon = [UIImage imageNamed:@"pinsEntertaiment"];
    marker.map = self.mapView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.missionsArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NGMissionData *mission = self.missionsArray[indexPath.row];
    int aValue = [[mission.time_start stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    int distanse = [[mission.distanse stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    
    NSInteger TimeStamp = aValue;

    NSDate *messageDatea = [NSDate dateWithTimeIntervalSince1970:TimeStamp];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    NSString *dateString = [dateFormatter stringFromDate:messageDatea];
    
    NSString *integerAsString = [@(distanse ) stringValue];
    NSString *distance = [NSString stringWithFormat:@"%@ %@ %@", @"at",integerAsString, @"km"];
    
    static NSString *standartIdentifier = @"standartCell";
    static NSString *advancedIdentifier = @"advancedCell";
    
    if (indexPath == self.selectedCell){
        self.imageURL = [NSURL URLWithString:@""];
        NGCustomTableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:advancedIdentifier];
        if(!cell){
            cell = [[NGCustomTableViewCell alloc] initWithStyle:
                    UITableViewCellStyleDefault      reuseIdentifier:advancedIdentifier];
        }
        
        int aValue = [[mission.time_start stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
        int andValue = [[mission.time_end stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
        
        NSInteger TimeStamp = aValue;
        NSInteger andTimeStamp = andValue;
        
        NSDate *messageDatea = [NSDate dateWithTimeIntervalSince1970:TimeStamp];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        
        NSString *dateString = [dateFormatter stringFromDate:messageDatea];
        
        NSDate *andMessageDatea = [NSDate dateWithTimeIntervalSince1970:andTimeStamp];
        NSDateFormatter* anddateFormatter = [[NSDateFormatter alloc] init];
        
        [anddateFormatter setDateStyle:NSDateFormatterMediumStyle];
        
        NSString *andDateString = [dateFormatter stringFromDate:andMessageDatea];
 
        cell.distancionLabel.text = distance;
        cell.distancLabel.text = distance;
        cell.dateLabel.text = dateString;
        cell.dateRealizationLabel.text = andDateString;
        cell.userLabel.text = self.allUsers[mission.creator][@"nickname"];
        self.IdUser = self.allUsers[mission.creator][@"id_user"];
        self.userAvatar = self.allUsers[mission.creator][@"photo"][@"url"];
        self.idSelectedMission = mission.id_mission;
        
        NSString * server = @"http://agent1.kievregion.net";
        NSString * avatar = [NSString stringWithFormat:@"%@%@", server, self.userAvatar];
        if (avatar) {
            self.imageURL = [NSURL URLWithString:avatar];
        }
        [self.userImageAvatar setImageWithURL:self.imageURL placeholderImage:[UIImage imageNamed:@"userimg"]];
        return cell;
    }
    NGCustomTableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:standartIdentifier];
    if(!cell){
        cell = [[NGCustomTableViewCell alloc] initWithStyle:
                UITableViewCellStyleDefault      reuseIdentifier:standartIdentifier];
    }
    cell.distancionLabel.text = distance;
    cell.dateMission.text = dateString;
    cell.descriptionLabel.text = mission.descriptionOne;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.selectedCell == indexPath){
        self.selectedCell = nil;
        [self.linksTableView reloadData];
        return;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationAutomatic];

    }];
    
    self.selectedCell = indexPath;
    [self.linksTableView reloadData];
    
}
#pragma mark - Accept Button
- (IBAction)acceptButtonTable:(id)sender {
    
    [self openMoreInfo:(id) sender];
    
}

- (void) setHighlighted:(BOOL) highlighted animated:(BOOL) animated {
    
    //    [super setHighlighted:highlighted animated:animated];
    //
    //    if (highlighted)
    //    {
    //        self.imageView.highlightedImage = [UIImage imageNamed:@"loginwithFacebook-1"];
    //    }
    //    else {
    //        self.imageView.highlightedImage = [UIImage imageNamed:@"header"];
    //    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath == self.selectedCell){
        
        return 240;
    }
    return 44;
}

-(void)openMoreInfo:(id) sender{
    
    if ([[self.infoView subviews] containsObject:(UIButton *)sender]){
        [UIView animateWithDuration:0.5 animations:^{
            [self.infoView setFrame:CGRectMake(self.mapView.center.x - 125, CGRectGetHeight(self.mapView.frame), 250, 150)];
        } completion:^(BOOL finished) {
            [self.infoView removeFromSuperview];
        }];
    }
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.acceptMissionsView setFrame:CGRectMake(self.mapView.center.x - 140, 100, 280, 310)];
        
        [self.acceptMissionsView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.acceptMissionsView updateConstraintsIfNeeded];
    }];
    
}

-(void)hideAction:(id) sender{
    if ([[self.infoView subviews] containsObject:(UIButton *)sender])
        [UIView animateWithDuration:0.5 animations:^{
            [self.infoView setFrame:CGRectMake(self.mapView.center.x - 125, CGRectGetHeight(self.mapView.frame), 250, 150)];
            
        } completion:^(BOOL finished) {
            [self.infoView removeFromSuperview];
        }];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}
-(void) hideAcceptView:(id)sender{
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.acceptMissionsView setFrame:CGRectMake(120, -320, 0, 0)];
        
        [self.acceptMissionsView layoutIfNeeded];
    } completion:^(BOOL finished) {

    }];
}
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.infoView setFrame:CGRectMake(mapView.center.x - 125, CGRectGetHeight(self.mapView.frame), 250, 150)];
        
    } completion:^(BOOL finished) {
        [self.infoView removeFromSuperview];
    }];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.acceptMissionsView setFrame:CGRectMake(120, -320, 0, 0)];
        
        [self.acceptMissionsView layoutIfNeeded];
    } completion:^(BOOL finished) {
}];
    
    
    
}

#pragma Mark - View information in info window

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{

    for (NGMissionData *mission in self.missionsArray){
        if (marker.position.longitude == [mission.longitude doubleValue] && marker.position.latitude == [mission.latitude doubleValue]){
            
//            self.imageURL = [NSURL URLWithString:@""];
            
            int aValue = [[mission.time_start stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
            int andValue = [[mission.time_end stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
            int distanse = [[mission.distanse stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    
            NSInteger TimeStamp = aValue;
            NSInteger andTimeStamp = andValue;

            NSDate *messageDatea = [NSDate dateWithTimeIntervalSince1970:TimeStamp];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            
            NSString *dateString = [dateFormatter stringFromDate:messageDatea];
            
            NSDate *andMessageDatea = [NSDate dateWithTimeIntervalSince1970:andTimeStamp];
            NSDateFormatter* anddateFormatter = [[NSDateFormatter alloc] init];
            
            [anddateFormatter setDateStyle:NSDateFormatterMediumStyle];
            
            NSString *andDateString = [dateFormatter stringFromDate:andMessageDatea];
            
            NSString *integerAsString = [@(distanse ) stringValue];
            NSString *distance = [NSString stringWithFormat:@"%@ %@ %@", @"at",integerAsString, @"km"];
            
            self.infoView.labelInfoMission.text = distance;
            self.infoView.descriptionLabel.text = mission.descriptionOne;
            self.infoView.dateLabel.text = (NSString*) andDateString;
            self.infoView.datePlaceLabel.text = (NSString*) dateString;
            self.infoView.userLabel.text = self.allUsers[mission.creator][@"nickname"];
            self.IdUser = self.allUsers[mission.creator][@"id_user"];
            self.userAvatar = self.allUsers[mission.creator][@"photo"][@"url"];
            self.idSelectedMission = mission.id_mission;
            NSString * server = @"http://agent1.kievregion.net";
            NSString * avatar = [NSString stringWithFormat:@"%@%@", server, self.userAvatar];
            if (avatar) {
                self.imageURL = [NSURL URLWithString:avatar];
            }
            [self.userImageAvatar setImageWithURL:self.imageURL placeholderImage:[UIImage imageNamed:@"userimg"]];
            break;
        }
    
    }

    [UIView animateWithDuration:0.5 animations:^{
        
        [self.infoView setFrame:CGRectMake(mapView.center.x - 125, CGRectGetHeight(self.mapView.frame) - 400, 250, 180)];
        
    }];
    [self.mapView addSubview:self.infoView];
    return YES;
    
}
- (UIView *)mapView:(GMSMapView *)mapView markerInfoContents:(GMSMarker *)marker{
    
    CustomInfoWindow *InfoWindow =  [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] objectAtIndex:0];
    InfoWindow.labelInfo.text = marker.title;
    InfoWindow.pinLocation = marker.position;
    
    
    return InfoWindow.acceptButton;
}
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    
    CustomInfoWindow *InfoWindow =  [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] objectAtIndex:0];
    InfoWindow.labelInfo.text = marker.title;
    InfoWindow.pinLocation = marker.position;
    
    [InfoWindow addSubview:InfoWindow.acceptButton];
    
    return InfoWindow;
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    
    NSLog(@"Hello");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)showMap:(id)sender {
    
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.acceptMissionsView setFrame:CGRectMake(120, -320, 0, 0)];
        
        [self.acceptMissionsView layoutIfNeeded];
    } completion:^(BOOL finished) {
}];
 self.mapView.hidden = NO;
    
    
}
- (IBAction)showList:(id)sender {
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.acceptMissionsView setFrame:CGRectMake(120, -320, 0, 0)];
        
        [self.acceptMissionsView layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
    self.mapView.hidden = YES;

}

- (void) showInfoOrUser:(id)sender {
    
    //infoUserVC
    
    NGDefultCurrentProfileInfo *infoUserVC =
    [self.storyboard instantiateViewControllerWithIdentifier:@"infoUserVC"];
    [self.navigationController presentViewController:infoUserVC animated:YES completion:nil];
    
//    CGFloat height = [UIScreen mainScreen].bounds.size.height;
//    CGFloat width = [UIScreen mainScreen].bounds.size.width;
////    self.topConstrainToVisualEffects.constant = 0;
////    self.heighVisualEffectConstrain.constant = height;
//    
//    [UIView animateWithDuration:0.4
//                          delay:0
//                        options:UIViewAnimationOptionBeginFromCurrentState
//                     animations:^(void) {
//                         self.topConstrainToVisualEffects.constant = 0;
//                         self.heighVisualEffectConstrain.constant = height;
//                         [self.view setNeedsLayout];
//                         [self.view layoutIfNeeded];
//                     }
//     
//                     completion:NULL];
//
//
//    [[NGServerManager sharedManager]
//     addId_user:self.IdUser
//     
//     onSuccess:^(NSDictionary *dict) {
//         
//         self.infoUserDic = dict[@"profile"];
//   
//         
//        NSString* birthdate = dict[@"profile"][@"birthdate"];
//         if ( birthdate == (NSString *)[NSNull null] )
//         {
//             birthdate = @"";
//         }
//         NSString* car =       dict[@"profile"][@"car"];
//         if ( car == (NSString *)[NSNull null] )
//         {
//             car = @"";
//         }
//         NSString* name =      dict[@"profile"][@"name"];
//         if ( name == (NSString *)[NSNull null] )
//         {
//             name = @"";
//         }
//         NSString* nickname =  dict[@"profile"][@"nickname"];
//         if ( nickname == (NSString *)[NSNull null] )
//         {
//             nickname = @"";
//         }
//         NSString* role =      dict[@"profile"][@"role"];
//         if ( role == (NSString *)[NSNull null] )
//         {
//             role = @"";
//         }
//         NSString* surname =   dict[@"profile"][@"surname"];
//         if ( surname == (NSString *)[NSNull null] )
//         {
//             surname = @"";
//         }
//         
//         NSString *complete = [NSString stringWithFormat:@"%@ %@", name,
//                               surname];
//         
//         NSString* lol = @"c";
//         if (role == lol) {
//             role = @"Creator";
//         } else role = @"Provider";
//
//         
//         self.textResponce = dict[@"text"];
//         self.textResponce = dict[@"action"];
//         self.acceptMissionsView.commentTextField.text = @"";
//         self.acceptMissionsView.commentTextView.text = @"";
//         
//         self.nameLabel.text =     complete;
//         self.birtDateLabel.text = birthdate;
//         self.carNameLabel.text =  car;
//         //self.nicknameUserLabel.text = nickname;
//         self.roleUserLabel.text = role;
//         
//         [UIView animateWithDuration:0.4
//                               delay:0
//                             options:UIViewAnimationOptionBeginFromCurrentState
//                          animations:^(void) {
//
//                              self.footerConstrains.constant = height - height + 100;
//                              [self.view setNeedsLayout];
//                              [self.view layoutIfNeeded];
//                          }
//          
//                          completion:NULL];}
//         
//     onFailure:^(NSError *error) {
//         
//         NSLog(@"ERROR: %@", error);
//         
//     }];
}
- (IBAction)showUser:(id)sender {
    
    [self showInfoOrUser:(id)sender];

}

- (IBAction)hideUser:(id)sender {
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^(void) {
                         
                         CGFloat height = [UIScreen mainScreen].bounds.size.height;
                         self.footerConstrains.constant = height + 700;
                         [self.view setNeedsLayout];
                         [self.view layoutIfNeeded];
                     }
                                          completion:NULL];

}
@end