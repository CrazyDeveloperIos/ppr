//
//  NGMyMissionViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/29/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGCustomTableViewCell.h"
#import "NGMissionData.h"

@interface NGMyMissionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSMutableArray* friendsArray;
@property (nonatomic, strong) NSIndexPath *selectedCell;
@property (nonatomic, strong) NSMutableArray *missionsArray;
@property (weak, nonatomic) IBOutlet UITableView *linksTableView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSDictionary *allUsers;
@property (nonatomic, strong) NGMissionData* myMission;
@property (nonatomic ,strong) UIRefreshControl *refreshView;
@property (nonatomic ,strong) NSString *sort;
//@property (nonatomic, getter = idMission) NSString* idMission;
@property (nonatomic) NSString *idMission;

@end
