//
//  NGMessageData.m
//  Uni Bit
//
//  Created by Naz on 6/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGMessageData.h"

@implementation NGMessageData

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        self.from = [responseObject objectForKey:@"from"];
        self.id_dialog = [responseObject objectForKey:@"id_dialog"];
        self.id_msg = [responseObject objectForKey:@"id_msg"];
        self.text = [responseObject objectForKey:@"text"];
        self.time_create = [responseObject objectForKey:@"time_create"];
        self.to = [responseObject objectForKey:@"to"];
        
        
        self.id_user = [responseObject objectForKey:@"id_user"];
        self.name = [responseObject objectForKey:@"name"];
        self.nickname = [responseObject objectForKey:@"nickname"];
        self.photoID = [responseObject objectForKey:@"photoID"];
        self.photoURL = [responseObject objectForKey:@"photoURL"];
        self.status = [responseObject objectForKey:@"status"];
        
        
           }
    return self;
}

-(NSString *)description   {
    return [NSString stringWithFormat:@"from %@, id_dialog %@, id_msg %@, text %@, time_create %@, to%@", self.from, self.id_dialog, self.id_msg ,self.text, self.time_create, self.to];
}

@end
