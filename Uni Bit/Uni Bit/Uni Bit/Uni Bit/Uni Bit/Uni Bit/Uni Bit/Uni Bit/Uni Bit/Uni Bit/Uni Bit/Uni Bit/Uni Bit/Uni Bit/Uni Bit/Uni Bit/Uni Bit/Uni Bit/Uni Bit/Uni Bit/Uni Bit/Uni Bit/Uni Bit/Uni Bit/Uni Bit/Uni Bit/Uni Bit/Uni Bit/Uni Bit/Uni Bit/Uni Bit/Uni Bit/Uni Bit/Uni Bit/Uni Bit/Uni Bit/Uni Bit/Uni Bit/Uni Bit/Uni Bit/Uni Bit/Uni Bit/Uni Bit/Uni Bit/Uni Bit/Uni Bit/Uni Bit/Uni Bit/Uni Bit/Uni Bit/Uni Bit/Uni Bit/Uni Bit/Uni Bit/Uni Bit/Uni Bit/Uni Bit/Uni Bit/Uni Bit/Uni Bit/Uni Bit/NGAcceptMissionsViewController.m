//
//  NGAcceptMissionsViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/16/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGMissionData.h"
#import "NGServetObject.h"
#import "NGAcceptMissionsViewController.h"
#import "NGViewControllerMapTable.h"


@interface NGAcceptMissionsViewController ()<CLLocationManagerDelegate, UITableViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIView *acceptMissionsViewController;
@property (nonatomic, strong) NSMutableArray *missionsArray;

@end

@implementation NGAcceptMissionsViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self =  [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        self.view.frame = CGRectMake(15, 0, 280, 310);
        [self setPreferredContentSize:CGSizeMake(280, 330)];
    }
    return self;
}
- (IBAction)closeAcceptWindow:(id)sender {
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.acceptMissionsViewController setFrame:CGRectMake(20, -320, 280, 310)];
        
        [self.acceptMissionsViewController layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.acceptMissionsViewController updateConstraintsIfNeeded];
    }];
}
-(void)closeAccept:(id) sender {
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.acceptMissionsViewController setFrame:CGRectMake(20, -320, 280, 310)];
        
        [self.acceptMissionsViewController layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.acceptMissionsViewController updateConstraintsIfNeeded];
    }];

}

- (void) refreshPins{

   // for (NGMissionData *mission in self.missionsArray){
        //self. = CLLocationCoordinate2DMake([mission.latitude doubleValue], [mission.longitude doubleValue]);

        
    //}
    
}

- (IBAction)sendCommentAcceptView:(id)sender {
    NSLog(@"vfivhd");
}

- (void)viewDidLoad {
    [self refreshPins];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.acceptMissionsViewController = [[self.storyboard instantiateViewControllerWithIdentifier:@"AcceptViewController"] view];
    [self.acceptMissionsViewController setFrame:CGRectMake(70,-300, 0, 0)];
    UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideAction:)];
    [taps setNumberOfTapsRequired:1];
    [self.acceptMissionsViewController addGestureRecognizer:taps];
    [self.view addSubview:self.acceptMissionsViewController];
    
}
-(void)hideAction:(id) sender{
    if ([[self.acceptMissionsViewController subviews] containsObject:(UIButton *)sender])
        [UIView animateWithDuration:0.5 animations:^{
            [self.acceptMissionsViewController setFrame:CGRectMake(0, 0, 320, 250)];
            
        } completion:^(BOOL finished) {
            [self.acceptMissionsViewController removeFromSuperview];
        }];
}

@end
