//
//  NGCustomTableViewCell.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/8/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGCustomTableViewCell.h"

@implementation NGCustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
