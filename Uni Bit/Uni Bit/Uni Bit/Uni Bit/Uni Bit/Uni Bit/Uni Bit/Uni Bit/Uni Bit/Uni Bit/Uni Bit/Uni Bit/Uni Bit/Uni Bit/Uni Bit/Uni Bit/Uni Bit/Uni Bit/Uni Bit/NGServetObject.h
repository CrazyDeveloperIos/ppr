//
//  NGServetObject.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/29/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGServetObject : NSObject

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@end
