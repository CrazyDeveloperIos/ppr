//
//  NGStartViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/5/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKBridgeAPIRequest.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "NGServerManager.h"

@interface NGStartViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emalTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)connectionButton:(id)sender;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;

@end

