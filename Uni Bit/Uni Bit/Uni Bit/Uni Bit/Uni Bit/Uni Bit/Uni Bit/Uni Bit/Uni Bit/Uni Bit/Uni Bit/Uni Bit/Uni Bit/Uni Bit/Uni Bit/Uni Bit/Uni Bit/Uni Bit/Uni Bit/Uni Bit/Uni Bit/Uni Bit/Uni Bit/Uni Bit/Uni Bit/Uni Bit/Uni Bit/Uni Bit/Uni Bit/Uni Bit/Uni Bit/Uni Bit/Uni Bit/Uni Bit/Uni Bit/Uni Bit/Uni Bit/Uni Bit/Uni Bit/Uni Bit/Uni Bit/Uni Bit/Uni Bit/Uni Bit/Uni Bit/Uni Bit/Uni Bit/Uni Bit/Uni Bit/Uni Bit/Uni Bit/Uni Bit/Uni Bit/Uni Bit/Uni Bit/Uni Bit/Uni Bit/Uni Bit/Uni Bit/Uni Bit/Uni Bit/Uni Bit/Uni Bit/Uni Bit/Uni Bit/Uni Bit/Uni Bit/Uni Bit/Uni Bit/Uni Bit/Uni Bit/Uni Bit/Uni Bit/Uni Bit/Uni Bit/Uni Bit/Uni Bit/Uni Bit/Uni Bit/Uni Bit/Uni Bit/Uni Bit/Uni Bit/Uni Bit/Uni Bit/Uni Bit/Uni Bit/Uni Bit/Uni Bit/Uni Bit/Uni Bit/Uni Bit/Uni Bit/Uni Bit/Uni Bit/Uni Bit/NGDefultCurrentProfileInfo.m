//
//  NGDefultCurrentProfileInfo.m
//  Uni Bit
//
//  Created by Naz on 6/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGDefultCurrentProfileInfo.h"

@interface NGDefultCurrentProfileInfo ()
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIButton *buttonOK;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation NGDefultCurrentProfileInfo

- (void)viewDidLoad {
    self.sendButton.layer.cornerRadius = 5;
    self.sendButton.layer.masksToBounds = YES;
    self.buttonOK.layer.cornerRadius = 15;
    self.buttonOK.layer.masksToBounds = YES;
    self.userImage.layer.cornerRadius = CGRectGetHeight(self.userImage.bounds)/2;
    self.userImage.layer.masksToBounds = YES;
    self.viewBackground.layer.cornerRadius = 5;
    self.viewBackground.layer.masksToBounds = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)okButton:(id)sender {

     [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
