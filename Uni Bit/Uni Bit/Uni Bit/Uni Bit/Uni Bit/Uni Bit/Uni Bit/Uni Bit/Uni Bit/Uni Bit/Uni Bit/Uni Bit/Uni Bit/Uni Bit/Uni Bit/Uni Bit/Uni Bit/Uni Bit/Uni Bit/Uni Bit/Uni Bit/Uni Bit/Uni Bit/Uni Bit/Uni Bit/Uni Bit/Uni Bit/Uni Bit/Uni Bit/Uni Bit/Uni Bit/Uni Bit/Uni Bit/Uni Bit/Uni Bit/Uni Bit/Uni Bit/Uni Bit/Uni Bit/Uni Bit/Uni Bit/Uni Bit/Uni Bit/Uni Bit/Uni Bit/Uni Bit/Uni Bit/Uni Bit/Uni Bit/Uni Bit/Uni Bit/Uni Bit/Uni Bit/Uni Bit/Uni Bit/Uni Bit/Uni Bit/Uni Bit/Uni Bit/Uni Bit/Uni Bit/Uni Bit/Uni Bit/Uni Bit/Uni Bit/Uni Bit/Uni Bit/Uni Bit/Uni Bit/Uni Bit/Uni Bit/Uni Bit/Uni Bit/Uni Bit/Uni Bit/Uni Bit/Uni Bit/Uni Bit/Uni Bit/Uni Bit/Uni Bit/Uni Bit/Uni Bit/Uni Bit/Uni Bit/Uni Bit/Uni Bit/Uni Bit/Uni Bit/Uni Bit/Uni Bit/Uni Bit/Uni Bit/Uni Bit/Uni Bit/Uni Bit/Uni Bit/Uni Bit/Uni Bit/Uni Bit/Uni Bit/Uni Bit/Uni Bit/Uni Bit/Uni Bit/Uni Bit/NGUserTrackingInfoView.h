//
//  MZUserTrackingInfoViewController.h
//  Мій Житомир
//
//  Created by Vladimir Matukh on 4/4/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGViewControllerMapTable.h"

@interface NGUserTrackingInfoView : UIView<GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelInfoMission;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *datePlaceLabel;
@property (weak, nonatomic) IBOutlet UIButton *showUserInfoInMap;

@end
