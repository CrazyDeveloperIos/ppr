//
//  NGcustomUserAcceptVC.m
//  Uni Bit
//
//  Created by Naz on 6/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGcustomUserAcceptVC.h"
#import "NGMyMissionViewController.h"
#import "NGServerManager.h"
#import "AFNetworking.h"
#import "NGAccessToken.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"


@interface NGcustomUserAcceptVC ()

@property (strong, nonatomic) NSString *textResponce;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSArray* messageParty;
@property (strong, nonatomic) NSMutableArray* arrayMessage;
@property (strong, nonatomic) NSDictionary* userParty;
@property (nonatomic, strong) NSDictionary *allUsers;
@property (strong, nonatomic) NSDictionary* user;
@property (strong, nonatomic) id obj;
@property (nonatomic ,strong) UIRefreshControl *refreshView;

@end

@implementation NGcustomUserAcceptVC

- (void)viewDidLoad {
    self.arrayMessage = [NSMutableArray new];
    [self setAcceptedUser];
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;
    self.refreshView = [[UIRefreshControl alloc] init];
    
    [self.refreshView addTarget:self action:@selector(setAcceptedUser) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshView];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)okButton:(id)sender {

    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];


}

- (void) setAcceptedUser {
    NGMyMissionViewController* Mission = [[NGMyMissionViewController alloc]init];
    NSString* idMission = Mission.idMission;
    
    [[NGServerManager sharedManager]
     addid_mission:idMission
     addnum:       @"1"
     addperpage:   @"4"
     
     
     onSuccess:^(NSDictionary *dict) {
         [self.missionsArray removeAllObjects];
         
         
         NSArray *missions = dict[@"missionParty"];
         for (NSDictionary *mission in missions){
             NGMissionData *missionData = [[NGMissionData alloc] initWithServerResponse:mission];
             [self.missionsArray addObject:missionData];
         }
         self.allUsers = dict[@"usersShort"];
         [self.tableView reloadData];
         [self.refreshView endRefreshing];
         
         
         
         
         [UIView animateWithDuration:0.4
                               delay:0
                             options:UIViewAnimationOptionBeginFromCurrentState
                          animations:^(void) {
                          }
          
                          completion:NULL];
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         [self.refreshView endRefreshing];
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"" subTitle:@"Not internet connection" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];
    
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMessage.count;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NGMessageData *message = self.arrayMessage[indexPath.row];
    NGMessageData *message = self.arrayMessage[indexPath.row];
    static NSString *simpleTableIdentifier = @"Cell";
    NGCustomDialogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    if(!cell){
        cell = [[NGCustomDialogTableViewCell alloc] initWithStyle:
                UITableViewCellStyleDefault      reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.nameUser.text = self.user[@"nickname"];
    cell.textMessage.text = message.text;
    //cell.userImage.image;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


@end
