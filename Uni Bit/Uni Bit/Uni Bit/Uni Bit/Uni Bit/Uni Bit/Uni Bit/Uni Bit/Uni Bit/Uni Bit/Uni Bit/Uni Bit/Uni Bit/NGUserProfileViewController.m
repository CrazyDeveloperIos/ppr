//
//  NGUserProfileViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 6/17/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGUserProfileViewController.h"
#import "NGServerManager.h"
#import "AFNetworking.h"
#import "NGAccessToken.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"

@interface NGUserProfileViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageUser;
@property (weak, nonatomic) IBOutlet UILabel *labelStatusUser;
@property (weak, nonatomic) IBOutlet UISwitch *switchUser;
@property (weak, nonatomic) NSString *statusUser;
@property (weak, nonatomic) NSString *filePach;
@property (weak, nonatomic) NSURL *imageURL;
@property (nonatomic, strong) NSDictionary *allUsers;
@end

@implementation NGUserProfileViewController

- (void)viewDidLoad {
//    
//    self.imageURL = [NSURL URLWithString:@"http://agent1.kievregion.net/img/22/avatar/221434671083ozoujpeb.jpeg"];
//    
//    
//    [self.imageUser setImageWithURL:self.imageURL placeholderImage:[UIImage imageNamed:@"userimg"]];
//    
//    
//    
    
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    
    
    
    self.imageUser.layer.cornerRadius = CGRectGetHeight(self.imageUser.bounds)/2;
    self.imageUser.layer.masksToBounds = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)uploadPhoto:(id)sender {
    
    //[self updateAvatar];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageUser.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.filePach = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Image.png"];
    
    // Save image.
    [UIImagePNGRepresentation(chosenImage) writeToFile:self.filePach atomically:YES];
    
    [self updateAvatar];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)takePhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}


- (void) updateAvatar {
    
    [[NGServerManager sharedManager]
     addAlbom:@"avatar"
     addFilePath:self.filePach
     
     onSuccess:^(NSDictionary *dict) {
         
         
         NSString * avaUser = dict[@"photo"][@"url"];
         NSString * server = @"http://agent1.kievregion.net";
         NSString * avatar = [NSString stringWithFormat:@"%@%@", server, avaUser];
         if (avatar) {
             self.imageURL = [NSURL URLWithString:avatar];
         }
         
         [self.imageUser setImageWithURL:self.imageURL placeholderImage:[UIImage imageNamed:@"userimg"]];
         
         
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"Ohh" subTitle:@"You add bad login or password" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];

}

- (IBAction)saveChangesButton:(id)sender {
    
    [[NGServerManager sharedManager]
     
     addUser_name:@"petro"
     addUser_surname:@"poroshenko"
     addNickname:@"petro_poroshenko"
     addBirthdate:@"07/17/1990"
     addEmail:@"petro_poproshenko@mail.ru"
     addId_fb:@"861867170919"
     addStatus:@"1"
     addRole:self.statusUser
     addCar:@"AA4537"
     
    
     onSuccess:^(NSDictionary *dict) {
     
         SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
         
         [alert showSuccess:@"" subTitle:@"Mission Success" closeButtonTitle:@"Done" duration:3.0f];
         
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"Ohh" subTitle:@"You add bad login or password" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];
    

    
}
- (IBAction)changeStatusUserSwitch:(id)sender {
    if (self.switchUser.on) {
     self.labelStatusUser.text = @"YES";
        self.statusUser = @"p";
    }
    else {
        self.labelStatusUser.text = @"NO";
        self.statusUser = @"c";
    }
}
@end
