//
//  NGCustomDialogTableViewCell.m
//  Uni Bit
//
//  Created by Naz on 6/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGCustomDialogTableViewCell.h"

@implementation NGCustomDialogTableViewCell

- (void)awakeFromNib {
    self.userImage.layer.cornerRadius = CGRectGetHeight(self.userImage.bounds)/2;
    self.userImage.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
