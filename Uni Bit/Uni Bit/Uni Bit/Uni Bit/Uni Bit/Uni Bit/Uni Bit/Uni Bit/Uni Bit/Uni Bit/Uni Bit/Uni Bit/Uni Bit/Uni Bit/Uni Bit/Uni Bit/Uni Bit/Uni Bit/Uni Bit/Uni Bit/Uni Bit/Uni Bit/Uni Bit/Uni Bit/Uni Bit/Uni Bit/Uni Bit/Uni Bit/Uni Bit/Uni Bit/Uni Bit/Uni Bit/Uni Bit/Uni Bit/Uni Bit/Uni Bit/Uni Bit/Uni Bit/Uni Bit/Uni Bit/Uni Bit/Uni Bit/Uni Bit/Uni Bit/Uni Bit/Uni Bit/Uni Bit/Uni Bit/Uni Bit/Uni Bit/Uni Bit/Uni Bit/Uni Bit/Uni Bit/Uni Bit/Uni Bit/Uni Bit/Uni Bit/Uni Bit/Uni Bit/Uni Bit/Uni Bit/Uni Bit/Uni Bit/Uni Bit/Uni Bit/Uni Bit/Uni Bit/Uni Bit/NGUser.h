//
//  NGUser.h
//  Uni Bit
//
//  Created by Naz on 6/29/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGUser : NSObject

@property (strong, nonatomic) NSString* from;
@property (strong, nonatomic) NSString* id_dialog;
@property (strong, nonatomic) NSString* id_msg;
@property (strong, nonatomic) NSString* text;
@property (strong, nonatomic) NSString* time_create;
@property (strong, nonatomic) NSString* to;

- (id) initWithServerResponse:(NSDictionary*) responseObject;
@end
