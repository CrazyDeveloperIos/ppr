//
//  NGcustomUserAcceptVC.h
//  Uni Bit
//
//  Created by Naz on 6/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGcustomUserAcceptVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *linksTableView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
