//
//  NSString+Array.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Array)
+ (NSString *)stringFromArray:(NSArray *) array;
@end
