//
//  NGCreateNewMissionViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGCreateNewMissionViewController.h"
#import "NGViewControllerMapTable.h"
#import <GoogleMaps/GoogleMaps.h>
#import "NSString+Array.h"
#import "NGServerManager.h"
#import "SCLAlertView.h"
#import "HSDatePickerViewController.h"


@interface NGCreateNewMissionViewController ()<GMSMapViewDelegate, UIGestureRecognizerDelegate , HSDatePickerViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *centerImage;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *visualEffectMapView;
@property (weak, nonatomic) IBOutlet UIButton *buttonDoneMap;

@property (weak, nonatomic) IBOutlet UITextField *decriptionMission;
@property (strong, nonatomic) NSString* setDecriptionMission;
@property (weak, nonatomic) IBOutlet UITextView *mapAddressView;

@property (weak, nonatomic) IBOutlet UITextField *descriptionPlace;
@property (strong, nonatomic) NSString* setDescriptionPlace;

@property (weak, nonatomic) IBOutlet UITextField *createTitle;
@property (strong, nonatomic) NSString* setCreateTitle;
@property (weak, nonatomic) IBOutlet UITextField *textFieldSearch;

@property (weak, nonatomic) IBOutlet UITextView *addresTextView;

@property (weak, nonatomic) IBOutlet UIButton *setStartDate;
@property (weak, nonatomic) IBOutlet UIButton *setEndDate;

@property (nonatomic, strong) NSDate *selectedDate;
@property (strong, nonatomic) NSDate* setDateS;
@property (strong, nonatomic) NSDate* setDateE;



@property (nonatomic) NSInteger setId_Place;
@property (strong, nonatomic) NSString* setTitleRegion;
@property (strong, nonatomic) NSString* setAddresRegion;
@property (strong, nonatomic) NSString* setLatitude;
@property (strong, nonatomic) NSString* setLongitude;
@end


@implementation NGCreateNewMissionViewController

- (void)viewDidLoad {
    
    self.visualEffectMapView.layer.cornerRadius = 5;
    self.visualEffectMapView.layer.masksToBounds = YES;
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
    self.setDateS = [NSDate date];
    self.setDateE = [NSDate date];
    [super viewDidLoad];
    self.mapView.myLocationEnabled = YES;
    //NSLog(@"User's location: %@", self.mapView.myLocation);
    self.mapView.mapType = kGMSTypeHybrid;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
    [self.setStartDate setTitle:[DateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    [self.setStartDate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.setEndDate setTitle:[DateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    [self.setEndDate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma Mark - Geocoding
- (IBAction)setStartDateButton:(id)sender {
    
    HSDatePickerViewController *hsdpvc = [HSDatePickerViewController new];
    hsdpvc.delegate = self;
    if (self.selectedDate) {
        hsdpvc.date = self.selectedDate;
    }
    [self presentViewController:hsdpvc animated:YES completion:nil];
    
}

- (IBAction)setEndDateButton:(id)sender {
    
    HSDatePickerViewController *hsdpvc = [HSDatePickerViewController new];
    hsdpvc.delegate = self;
    if (self.selectedDate) {
        hsdpvc.date = self.selectedDate;
    }
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate    {
    CLGeocoder *geocoder = [[CLGeocoder alloc]init];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks firstObject];
        
        NSString * myString = [NSString stringFromArray:placemark.addressDictionary[@"FormattedAddressLines"]];
        NSString * newString = [myString stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        newString = [newString stringByReplacingOccurrencesOfString:@"'\'" withString:@" "];
        newString = [newString stringByReplacingOccurrencesOfString:@"-" withString:@" "];
        
        self.setTitleRegion = [newString stringByReplacingOccurrencesOfString:@"'\'" withString:@" "];
        self.setAddresRegion = [newString stringByReplacingOccurrencesOfString:@"'\'" withString:@" "];
        
        self.addresTextView.text = [NSString stringFromArray:placemark.addressDictionary[@"FormattedAddressLines"]];
        self.mapAddressView.text = [NSString stringFromArray:placemark.addressDictionary[@"FormattedAddressLines"]];
        self.setLatitude =  [NSString stringWithFormat:@"%f", coordinate.latitude];
        self.setLongitude =  [NSString stringWithFormat:@"%f", coordinate.longitude];
        
        
        GMSMarker *markerSearchTo = [[GMSMarker alloc] init];
        markerSearchTo.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
        markerSearchTo.title = [NSString stringFromArray:placemark.addressDictionary[@"FormattedAddressLines"]];
        markerSearchTo.appearAnimation = kGMSMarkerAnimationPop;
        markerSearchTo.map = self.mapView;
    }];
}
- (IBAction)searchAdressTextField:(id)sender {
    
}


- (IBAction)showMapButton:(id)sender {
    
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^(void) {
                         
                         CGFloat width = [UIScreen mainScreen].bounds.size.width;
                         CGFloat height = [UIScreen mainScreen].bounds.size.height;
                         self.visualEffectMapView.frame = CGRectMake(0, 0 , width, height);
                     }
                         completion:NULL];
    
}
- (IBAction)doneVIewMap:(id)sender {
    //CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^(void) {
                         self.visualEffectMapView.frame = CGRectMake(0, height + 10, 320, 600);
                         
                     }
                     completion:NULL];
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"yyyy-MM-dd HH:mm";
    
    [self.setStartDate setTitle:[dateFormater stringFromDate:date] forState:UIControlStateNormal];
    [self.setStartDate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}




- (void) pushAction {
    HSDatePickerViewController *hsdpvc = [HSDatePickerViewController new];
    hsdpvc.delegate = self;
    if (self.selectedDate) {
        hsdpvc.date = self.selectedDate;
    }
    [self presentViewController:hsdpvc animated:YES completion:nil];
    
}

//optional
- (void)hsDatePickerDidDismissWithQuitMethod:(HSDatePickerQuitMethod)method {
    NSLog(@"Picker did dismiss with %lu", (unsigned long)method);
}

//optional
- (void)hsDatePickerWillDismissWithQuitMethod:(HSDatePickerQuitMethod)method {
    NSLog(@"Picker will dismiss with %lu", (unsigned long)method);
}

- (IBAction)registerMissionButton:(id)sender {
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMddyyyyHHmm"];
    
    
    NSString* startDate = [dateFormatter stringFromDate:self.setDateS];
    NSString* endDade = [dateFormatter stringFromDate:self.setDateE];
    
    self.setCreateTitle = [self.createTitle text];
    self.setDecriptionMission = [self.decriptionMission text];
    self.setDescriptionPlace = [self.descriptionPlace text];
    
    [[NGServerManager sharedManager]
     
     setTitle:self.setCreateTitle
     setTime_start:startDate
     setTime_end:endDade
     setPrice_start:@"10"
     setPrice_end:@"20"
     setDescription:self.setDecriptionMission
     setTitleRegion:@" jkbjkb jk,m"
     setAddressRegion:self.setAddresRegion
     setLatitude:self.setLatitude
     setLongitude:self.setLongitude
     setDescriptionTwo:@"ghfv yjhfv"
     
     
     onSuccess:^(NSDictionary *dict) {
         
         SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
         
         [alert showSuccess:@"" subTitle:@"Mission Success" closeButtonTitle:@"Done" duration:3.0f];
         
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"Ohh" subTitle:@"You add bad login or password" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];
    
    
    
}

@end

