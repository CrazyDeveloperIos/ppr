//
//  NGCustomTableViewCell.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/8/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGCustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *distancionLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateRealizationLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *distancLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateMission;

@end
