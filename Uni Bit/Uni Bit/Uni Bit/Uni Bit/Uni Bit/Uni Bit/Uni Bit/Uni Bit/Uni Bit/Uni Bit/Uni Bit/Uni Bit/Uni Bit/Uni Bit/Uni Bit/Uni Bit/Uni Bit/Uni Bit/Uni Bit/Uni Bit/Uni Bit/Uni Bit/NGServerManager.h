//
//  NGServerManager.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/13/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGServerManager : NSObject

+ (NGServerManager *) sharedManager;


#pragma mark - Registration in App

- (void) registration: (NSString *) email
            withLogin: (NSString *) login
          andPassword: (NSString *) password
            onSuccess: (void(^) (NSDictionary *dict)) success
            onFailure: (void(^) (NSError *error)) failure;


#pragma mark - Login User for Facebook

- (void)    withLoginUserFacebook: (NSString *) loginUserFacebook
                        onSuccess: (void(^) (NSDictionary *dict)) success
                        onFailure: (void(^) (NSError *error)) failure;



#pragma mark -  LoginUser und password

- (void)    withLoginUser: (NSString *) loginUser
          andPasswordUser: (NSString *) passwordUser
                onSuccess: (void(^) (NSDictionary *dict)) success
                onFailure: (void(^) (NSError *error)) failure;



#pragma mark - Logout

- (void)    withClickLogout: (NSString *) click
                  onSuccess: (void(^) (NSDictionary *dict)) success
                  onFailure: (void(^) (NSError *error)) failure;


#pragma mark - Create Mission

- (void)    setTitle: (NSString *) title
       setTime_start: (NSString *) time_start
         setTime_end: (NSString *) time_end
      setPrice_start: (NSString *) price_start
        setPrice_end: (NSString *) price_end
      setDescription: (NSString *) descriptionOne
      setTitleRegion: (NSString *)titleRegion
    setAddressRegion: (NSString *)addressRegion
         setLatitude: (NSString *) latitude
        setLongitude: (NSString *) longitude
   setDescriptionTwo: (NSString *) descriptionTwo
           onSuccess: (void(^) (NSDictionary *dict)) success
           onFailure: (void(^) (NSError *error)) failure;


#pragma mark - Forget Password

- (void) addOldPass: (NSString *) passwordOld
         addNewPass: (NSString *) passwordNew
         addConfirm: (NSString *) confirm
          onSuccess: (void(^) (NSDictionary *dict)) success
          onFailure: (void(^) (NSError *error)) failure;


#pragma mark - Get Mission

- (void)
        addCreator: (NSString *) creator
       addProvider: (NSString *) provider
            addNum: (NSString *) num
        addPerpage: (NSString *) perpage
      addSort_flow: (NSString *) sort_flow
        addSort_by: (NSString *) sort_by
  addUser_latitude: (NSString *) user_latitude
 addUser_longitude: (NSString *) user_longitude
            addDis: (NSString *) dis
        addDis_cut: (NSString *) dis_cut
        addstatusM: (NSString *) statusM
onSuccess: (void(^) (NSDictionary *dict)) success
onFailure: (void(^) (NSError *error)) failure;

#pragma mark - Upload photo

- (void)  addAlbom: (NSString *) albom
       addFilePath: (NSString *) filePath
         onSuccess: (void(^) (NSDictionary *dict)) success
         onFailure: (void(^) (NSError *error)) failure;

#pragma mark - Accept Send

- (void) addid_mission: (NSString *) id_mission
                addmsg: (NSString *) msg
              addprice: (NSString *) price
             onSuccess: (void(^) (NSDictionary *dict)) success
             onFailure: (void(^) (NSError *error)) failure;
#pragma mark - Get Dialog

- (void) addNum: (NSString *) num
     addPerpage: (NSString *) perpage
      onSuccess: (void(^) (NSDictionary *dict)) success
      onFailure: (void(^) (NSError *error)) failure;



#pragma mark - Profile Settings

- (void) addUser_name: (NSString *) user_name
      addUser_surname: (NSString *) user_surname
          addNickname: (NSString *) nickname
         addBirthdate: (NSString *) birthdate
             addEmail: (NSString *) email
             addId_fb: (NSString *) id_fb
            addStatus: (NSString *) status
              addRole: (NSString *) role
               addCar: (NSString *) car
            onSuccess: (void(^) (NSDictionary *dict)) success
            onFailure: (void(^) (NSError *error)) failure;

#pragma mark - Profile Show

- (void) addId_user: (NSString *) id_user
          onSuccess: (void(^) (NSDictionary *dict)) success
          onFailure: (void(^) (NSError *error)) failure;

#pragma mark - Send Message

- (void)         addto: (NSString *) to
                addmsg: (NSString *) msg
             addId_msg: (NSString *) id_msg
             onSuccess: (void(^) (NSDictionary *dict)) success
             onFailure: (void(^) (NSError *error)) failure;
#pragma mark - Show accepted user

- (void) addid_mission:  (NSString *) id_mission
                addnum:  (NSString *) num
            addperpage: (NSString *) perpage
             onSuccess: (void(^) (NSDictionary *dict)) success
             onFailure: (void(^) (NSError *error)) failure;
@end
