//
//  NGGeocodingService.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGGeocodingService : NSObject

- (id)init;

@property (nonatomic, strong) NSDictionary *geocode;

@end
