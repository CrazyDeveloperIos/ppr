//
//  settingViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/10/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGSettingViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end
