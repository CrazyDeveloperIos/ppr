//
//  CustomInfoWindow.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/7/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface CustomInfoWindow : UIView
@property (weak, nonatomic) IBOutlet UILabel *labelInfo;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageInfo;
@property (nonatomic) CLLocationCoordinate2D pinLocation;
@end
