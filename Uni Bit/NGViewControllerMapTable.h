//
//  NGViewControllerMapTable.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/6/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "NGMissionData.h"
#import "NGLeftPanelViewController.h"
#import "NSString+Array.h"

@interface NGViewControllerMapTable : UIViewController <GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (nonatomic ,strong) NSString *IdUser;

@end

