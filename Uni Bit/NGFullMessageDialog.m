//
//  NGFullMessageDialog.m
//  Uni Bit
//
//  Created by Naz on 7/3/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGFullMessageDialog.h"

@implementation NGFullMessageDialog

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        
        self.creator = [responseObject objectForKey:@"creator"];
        self.descriptionOne = [responseObject objectForKey:@"description"];
        self.distanse = [responseObject objectForKey:@"distanse"];
        self.id_mission = [responseObject objectForKey:@"id_mission"];
        self.reference_num = [responseObject objectForKey:@"reference_num"];
        self.status = [responseObject objectForKey:@"status"];
        self.time_end = [responseObject objectForKey:@"time_end"];
        self.time_start = [responseObject objectForKey:@"time_start"];
        self.title = [responseObject objectForKey:@"title"];
        self.type = [responseObject objectForKey:@"type"];
        
        self.addressPlace = responseObject[@"place"][@"address"];
        self.latitude = responseObject[@"place"][@"latitude"];
        self.longitude = responseObject[@"place"][@"longitude"];
        self.descriptionPlace = responseObject[@"place"][@"description"];
        self.id_Place = responseObject[@"place"][@"id_place"];
        self.titlePlace = responseObject[@"place"][@"title"];
        self.id_User = [responseObject objectForKey:@"nickname"];
 
    }
    return self;
}
-(NSString *)description   {
    return [NSString stringWithFormat:@"Title %@, Creator %@, Address %@", self.title, self.creator, self.addressPlace];
}

@end
