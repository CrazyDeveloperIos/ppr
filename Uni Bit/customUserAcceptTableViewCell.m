//
//  customUserAcceptTableViewCell.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 6/20/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "customUserAcceptTableViewCell.h"

@implementation customUserAcceptTableViewCell

- (void)awakeFromNib {
    self.imageProfile.layer.cornerRadius = CGRectGetHeight(self.imageProfile.bounds)/2;
    self.imageProfile.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
