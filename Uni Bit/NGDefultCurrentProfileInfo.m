//
//  NGDefultCurrentProfileInfo.m
//  Uni Bit
//
//  Created by Naz on 6/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGDefultCurrentProfileInfo.h"
#import "NGViewControllerMapTable.h"
#import "NGServerManager.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"

@interface NGDefultCurrentProfileInfo ()
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UILabel *nicknameUserLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIButton *buttonOK;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UILabel *roleUser;
@property (weak, nonatomic) IBOutlet UILabel *carUser;
@property (nonatomic, strong) NSString* idUser;
@property (nonatomic, strong) NSDictionary* infoUserDic;
@property (nonatomic, strong) NSString* textResponce;



@end

@implementation NGDefultCurrentProfileInfo

- (void)viewDidLoad {
    

    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:@"myevent" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:@"userAccepted" object:nil];
    self.sendButton.layer.cornerRadius = 5;  
    self.sendButton.layer.masksToBounds = YES;
    self.buttonOK.layer.cornerRadius = 15;
    self.buttonOK.layer.masksToBounds = YES;
    self.userImage.layer.cornerRadius = CGRectGetHeight(self.userImage.bounds)/2;
    self.userImage.layer.masksToBounds = YES;
    self.viewBackground.layer.cornerRadius = 5;
    self.viewBackground.layer.masksToBounds = YES;
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    if (notification.object != NULL)
    {
        self.idUser = notification.object[@"id"];
        [self getUserInfo];
    }
}

- (void) getUserInfo {
    
    [[NGServerManager sharedManager]
     
     addId_user:self.idUser
     
          onSuccess:^(NSDictionary *dict) {
     
              self.infoUserDic = dict[@"profile"];
     
     
             NSString* birthdate = dict[@"profile"][@"birthdate"];
              if ( birthdate == (NSString *)[NSNull null] )
              {
                  birthdate = @"";
              }
              NSString* car =       dict[@"profile"][@"car"];
              if ( car == (NSString *)[NSNull null] )
              {
                  car = @"";
              }
              NSString* name =      dict[@"profile"][@"name"];
              if ( name == (NSString *)[NSNull null] )
              {
                  name = @"";
              }
              NSString* nickname =  dict[@"profile"][@"nickname"];
              if ( nickname == (NSString *)[NSNull null] )
              {
                  nickname = @"";
              }
              NSString* role =      dict[@"profile"][@"role"];
              if ( role == (NSString *)[NSNull null] )
              {
                  role = @"";
              }
              NSString* surname =   dict[@"profile"][@"surname"];
              if ( surname == (NSString *)[NSNull null] )
              {
                  surname = @"";
              }
     
              NSString *complete = [NSString stringWithFormat:@"%@ %@", name,
                                    nickname];
     
              NSString* lol = @"c";
              if ([role isEqualToString:lol]) {
                  role = @"Creator";
              } else role = @"Provider";
              
              NSString *photo = dict[@"profile"][@"photo"][@"url"];
              if ( surname == (NSString *)[NSNull null] )
              {
                  surname = @"";
              }

              NSString * server = @"http://agent1.kievregion.net";
              NSString * avatar = [NSString stringWithFormat:@"%@%@", server, photo];
              NSURL* img;
              if (avatar) {
                  img = [NSURL URLWithString:avatar];
              }
              [self.userImage setImageWithURL:img placeholderImage:[UIImage imageNamed:@"userimg"]];
     
              self.textResponce = dict[@"text"];
              self.textResponce = dict[@"action"];
              self.nicknameUserLabel.text = complete;
              //self.birtDateLabel.text = birthdate;
              self.carUser.text =  car;
//              //self.nicknameUserLabel.text = nickname;
              self.roleUser.text = role;

          }
              
          onFailure:^(NSError *error) {
              
              NSLog(@"ERROR: %@", error);
              
          }];

}

- (IBAction)okButton:(id)sender {

     [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
