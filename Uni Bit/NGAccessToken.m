//
//  NGAccessToken.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGAccessToken.h"

@implementation NGAccessToken
-(instancetype)init{
    self = [super init];
    if (self){
        
    }
    return self;
}
-(void)setToken:(NSString *)token{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)token{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
}
@end
