//
//  NGAcceptedUserData.h
//  Uni Bit
//
//  Created by Naz on 6/29/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGServetObject.h"

@interface NGAcceptedUserData : NGServetObject
@property (strong, nonatomic) NSString* id_mission;
@property (strong, nonatomic) NSString* msg;
@property (strong, nonatomic) NSString* price;
@property (strong, nonatomic) NSString* provider;
@property (strong, nonatomic) NSString* time;
@property (strong, nonatomic) NSString* id_user;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* nickname;
@property (strong, nonatomic) NSString* photoID;
@property (strong, nonatomic) NSString* photoURL;
@property (strong, nonatomic) NSString* status;
@property (strong, nonatomic) NSString* role;

@end
