//
//  NGStartAppViewController.m
//  Uni Bit
//
//  Created by Naz on 28.07.15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGStartAppViewController.h"
#import "NGStartViewController.h"
#import "AMTumblrHud.h"
#import "SCLAlertView.h"
#import "NGServerManager.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface NGStartAppViewController ()

@end

@implementation NGStartAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getStatusUserInApp];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getStatusUserInApp {
    NGServerManager *connection = [NGServerManager  alloc];

    if(connection.connectedToInternet == NO)
    {
        // Not connected to the internet
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Oh!" subTitle:@"Please, check your Inretnet connection " closeButtonTitle:@"OK" duration:0.0f]; // Error
        
    }
    else
    {
        // Connected to the internet
    
    AMTumblrHud *tumblrHUD = [[AMTumblrHud alloc] initWithFrame:CGRectMake((CGFloat) ((self.view.frame.size.width - 55) * 0.5),
                                                                           (CGFloat) ((self.view.frame.size.height - 20) * 0.5), 55, 20)];
    tumblrHUD.hudColor = UIColorFromRGB(0xFF5000);
    [self.view addSubview:tumblrHUD];
    
    [tumblrHUD showAnimated:YES];
    
    
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    NSString *userName = [user objectForKey:@"userName"];
    NSString *password = [user objectForKey:@"password"];
    
    if (([userName length] == 0) || ([password length] == 0)) {
        self->myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                        target:self
                                                      selector:@selector(goToRegister)
                                                      userInfo:nil
                                                       repeats:NO];
        
        //[self goToRegister];
        [tumblrHUD hide];
    }
    else {
    
    [[NGServerManager sharedManager]
     withLoginUser:userName
     andPasswordUser:password
     onSuccess:^(NSDictionary *dict) {
         
         [tumblrHUD hide];
         if (dict[@"user"][@"photo"][@"url"] == [NSNull null]) {
             [user setObject:@"" forKey:@"photo"];
         } else [user setObject:dict[@"user"][@"photo"][@"url"] forKey:@"photo"];
         if (dict[@"user"][@"admin_check"] == [NSNull null]) {
             [user setObject:@"" forKey:@"admin_check"];
         } else [user setObject:dict[@"user"][@"admin_check"] forKey:@"admin_check"];
         if (dict[@"user"][@"card"] == [NSNull null]) {
             [user setObject:@"" forKey:@"card"];
         } else [user setObject:dict[@"user"][@"card"] forKey:@"card"];
         if (dict[@"user"][@"id_user"] == [NSNull null]) {
             [user setObject:@"" forKey:@"id_user"];
         } else [user setObject:dict[@"user"][@"id_user"] forKey:@"id_user"];
         if (dict[@"user"][@"email"] == [NSNull null]) {
             [user setObject:@"" forKey:@"email"];
         } else [user setObject:dict[@"user"][@"email"] forKey:@"email"];
         if (dict[@"user"][@"denied"] == [NSNull null]) {
             [user setObject:@"" forKey:@"denied"];
         } else [user setObject:dict[@"user"][@"denied"] forKey:@"denied"];
         if (dict[@"user"][@"birthdate"] == [NSNull null]) {
             [user setObject:@"" forKey:@"birthdate"];
         } else [user setObject:dict[@"user"][@"birthdate"] forKey:@"birthdate"];
         if (dict[@"user"][@"nickname"] == [NSNull null]) {
             [user setObject:@"" forKey:@"nickname"];
         } else [user setObject:dict[@"user"][@"nickname"] forKey:@"nickname"];
         if (dict[@"user"][@"role"] == [NSNull null]) {
             [user setObject:@"" forKey:@"role"];
         } else [user setObject:dict[@"user"][@"role"] forKey:@"role"];
         if (dict[@"user"][@"rating"][@"value"] == [NSNull null]) {
             [user setObject:@"0" forKey:@"rating"];
         } else [user setObject:dict[@"user"][@"rating"][@"value"] forKey:@"rating"];
         if (dict[@"user"][@"token"] == [NSNull null]) {
             [user setObject:@"" forKey:@"token"];
         } else [user setObject:dict[@"user"][@"token"] forKey:@"token"];
         if (dict[@"user"][@"name"] == [NSNull null]) {
             [user setObject:@"" forKey:@"name"];
         } else [user setObject:dict[@"user"][@"name"] forKey:@"name"];
         if (dict[@"user"][@"status"] == [NSNull null]) {
             [user setObject:@"" forKey:@"status"];
         } else [user setObject:dict[@"user"][@"status"] forKey:@"status"];
         if (dict[@"user"][@"surname"] == [NSNull null]) {
             [user setObject:@"" forKey:@"surname"];
         } else [user setObject:dict[@"user"][@"surname"] forKey:@"surname"];

         [user synchronize];
         
         NSString* role = dict[@"user"][@"role"];
         NSString* object = dict[@"object"];
        
         if ([object isEqualToString:@"error"]) {
             NSLog(@"error");
         }
         else {
             if ([role isEqualToString:@"p"]) {
                 
                 NSString * storyboardName = @"Provider";
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                 UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"RevealVC"];
                 [self showViewController:vc sender:self];
                 
             }
             else {
             
                 NSString * storyboardName = @"Creator";
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
             UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"RevealVC"];
             [self showViewController:vc sender:self];
             }
         }
         
         
     }
     
     onFailure:^(NSError *error) {
         [tumblrHUD hide];
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"Oh!" subTitle:@"Please, check your Inretnet connection " closeButtonTitle:@"OK" duration:0.0f]; // Error
         [self getStatusUserInApp];
         NSLog(@"ERROR: %@", error);
     }];
    }
    }
}

-(void) goToRegister {
//    if ([myTimer isValid]) {
//        [myTimer invalidate];
//    }
    NGStartViewController *loginVC =
    [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
    [self showViewController:loginVC sender:nil];
    //[self.navigationController presentViewController:loginVC animated:YES completion:nil];
    
}




@end
