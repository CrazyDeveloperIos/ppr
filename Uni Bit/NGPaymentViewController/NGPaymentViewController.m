//
//  NGPaymentViewController.m
//  Uni Bit
//
//  Created by Naz on 12.08.15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGPaymentViewController.h"
#import "SWRevealViewController.h"

@interface NGPaymentViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewEff;
@property (weak, nonatomic) IBOutlet UITextField *numberLongTextField;
@property (weak, nonatomic) IBOutlet UITextField *cvvLabel;

@end

@implementation NGPaymentViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewEff.layer.cornerRadius = 10;
    self.viewEff.layer.masksToBounds = YES;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        self.sidebarButton.action = @selector(revealToggle:);
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
    // Do any additional setup after loading the view.
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


//#pragma mark - UITextViewDelegate
//
//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    } else {
//        return YES;
//    }
//}
//
//#pragma mark - UITextFieldDelegate
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}
//
//#pragma mark - private
//
//- (UIToolbar *)createToolbar
//{
//    UIToolbar *toolBar = [UIToolbar new];
//    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextTextField)];
//    UIBarButtonItem *prevButton = [[UIBarButtonItem alloc] initWithTitle:@"Prev" style:UIBarButtonItemStylePlain target:self action:@selector(prevTextField)];
//    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(textFieldDone)];
//    toolBar.items = @[prevButton, nextButton, space, done];
//    [toolBar sizeToFit];
//    return toolBar;
//}
//
//- (void)nextTextField
//{
//    NSUInteger currentIndex = [[self inputViews] indexOfObject:self.view];
//    NSUInteger nextIndex = currentIndex + 1;
//    nextIndex += [[self inputViews] count];
//    nextIndex %= [[self inputViews] count];
//    UITextField *nextTextField = [[self inputViews] objectAtIndex:nextIndex];
//    [nextTextField becomeFirstResponder];
//}
//
//- (void)prevTextField
//{
//    NSUInteger currentIndex = [[self inputViews] indexOfObject:self.view];
//    NSUInteger prevIndex = currentIndex - 1;
//    prevIndex += [[self inputViews] count];
//    prevIndex %= [[self inputViews] count];
//    UITextField *nextTextField = [[self inputViews] objectAtIndex:prevIndex];
//    [nextTextField becomeFirstResponder];
//}
//
//- (void)textFieldDone
//{
//    [self.view resignFirstResponder];
//}
//
//- (NSArray *)inputViews
//{
//    NSMutableArray *returnArray = [NSMutableArray array];
//    for (UIView *eachView in self.view.subviews) {
//        //if ([eachView respondsToSelector:@selector(setText:)]) {
//        if ([eachView isKindOfClass:[UITextView class]]) {
//            [returnArray addObject:eachView];
//        }
//    }
//    return returnArray;
//}



//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Text entered:"
//                                                    message:self.numberLongTextField.text
//                                                   delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
//    
//    return YES;
//}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    
    NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSArray* components = [string componentsSeparatedByCharactersInSet:validationSet];
    
    if ([components count] > 1) {
        return NO;
    }
    
    NSString* newString = [self.numberLongTextField.text stringByReplacingCharactersInRange:range withString:string];
    
    //XXXX-XXXX-XXXX-XXXX
    
    NSLog(@"new string = %@", newString);
    
    NSArray* validComponents = [newString componentsSeparatedByCharactersInSet:validationSet];
    
    newString = [validComponents componentsJoinedByString:@""];
    
    // XXXXXXXXXXXX
    
    NSLog(@"new string fixed = %@", newString);
    static const int firstNumberMaxLength = 4;
    static const int localNumberMaxLength = 4;
    static const int areaCodeMaxLength = 4;
    static const int countryCodeMaxLength = 4;
    
    if ([newString length] > localNumberMaxLength + areaCodeMaxLength + countryCodeMaxLength + firstNumberMaxLength) {
        return NO;
    }
    
    
    NSMutableString* resultString = [NSMutableString string];
    
    NSInteger localNumberLength = MIN([newString length], localNumberMaxLength);
    
    if (localNumberLength > 0) {
        
        NSString* number = [newString substringFromIndex:(int)[newString length] - localNumberLength];
        
        [resultString appendString:number];
        
        if ([resultString length] > 4) {
            [resultString insertString:@"-" atIndex:4];
        }
        
    }
    
    if ([newString length] > localNumberMaxLength) {
        
        NSInteger areaCodeLength = MIN((int)[newString length] - localNumberMaxLength, areaCodeMaxLength);
        
        NSRange areaRange = NSMakeRange((int)[newString length] - localNumberMaxLength - areaCodeLength, areaCodeLength);
        
        NSString* area = [newString substringWithRange:areaRange];
        
        area = [NSString stringWithFormat:@"-%@-", area];
        
        [resultString insertString:area atIndex:0];
    }
    
    if ([newString length] > localNumberMaxLength + areaCodeMaxLength) {
        
        NSInteger countryCodeLength = MIN((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
        
        NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
        
        NSString* countryCode = [newString substringWithRange:countryCodeRange];
        
        countryCode = [NSString stringWithFormat:@"-%@", countryCode];
        
        [resultString insertString:countryCode atIndex:0];
    }
    if ([newString length] > localNumberMaxLength + areaCodeMaxLength + firstNumberMaxLength) {
        
        NSInteger countryCodeLength = MIN((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
        
        NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
        
        NSString* countryCode = [newString substringWithRange:countryCodeRange];
        
        countryCode = [NSString stringWithFormat:@"%@", countryCode];
        
        [resultString insertString:countryCode atIndex:0];
    }
    
    textField.text = resultString;
    
    return NO;
}

@end
