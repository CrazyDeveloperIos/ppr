//
//  NGPaymentViewController.h
//  Uni Bit
//
//  Created by Naz on 12.08.15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextFieldWithLimit.h"


@interface NGPaymentViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
