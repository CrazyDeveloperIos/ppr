//
//  NGUserInfo.m
//  Uni Bit
//
//  Created by Naz on 24.08.15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGUserInfo.h"

@implementation NGUserInfo

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"statusUser", @"role", @"name", @"surname", @"nickname", @"birthdate", @"email", @"status", @"filePath", @"admin_check", @"card", @"id_user", @"denied", @"rating", @"adress", @"adminCheck", @"bicycle", @"bus", @"car", @"carexist", @"city", @"country", @"creditCard", @"creditValidation", @"licensec", @"licensep", @"moto", @"other", @"phone", @"points", @"ship", @"truck", @"van"]];
        [mapping mapKeyPath:@"imageURL" toProperty:@"imageURL"
             withValueBlock:[EKMappingBlocks urlMappingBlock]
               reverseBlock:[EKMappingBlocks urlReverseMappingBlock]];
        
        
        [mapping mapKeyPath:@"name" toProperty:@"name" withValueBlock:^id(NSString *key, id value) {
            return value;
        } reverseBlock:^id(id value){
            return  value ? value : @"";
        }];
        [mapping mapKeyPath:@"city" toProperty:@"city" withValueBlock:^id(NSString *key, id value) {
            return value;
        } reverseBlock:^id(id value){
            return  value ? value : @"";
        }];
        [mapping mapKeyPath:@"truck" toProperty:@"truck" withValueBlock:^id(NSString *key, id value) {
            return value;
        } reverseBlock:^id(id value){
            return  value ? value : @"";
        }];
       
        
    }];
}

@end
