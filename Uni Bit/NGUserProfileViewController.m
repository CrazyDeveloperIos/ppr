//
//  NGUserProfileViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 6/17/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGUserProfileViewController.h"
#import "NGServerManager.h"
#import "AFNetworking.h"
#import "NGAccessToken.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"
#import "NGUserInfo.h"
@interface NGUserProfileViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageUser;
@property (weak, nonatomic) IBOutlet UILabel *labelStatusUser;
@property (weak, nonatomic) IBOutlet UISwitch *switchUser;
@property (strong, nonatomic) NSString *statusUser;
@property (strong, nonatomic) NSString *role;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *surname;
@property (strong, nonatomic) NSString *nickname;
@property (strong, nonatomic) NSString *birthdate;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *filePach;
@property (strong, nonatomic) NSString *admin_check;
@property (strong, nonatomic) NSString *card;
@property (strong, nonatomic) NSString *id_user;
@property (strong, nonatomic) NSString *denied;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *adminCheck;
@property (strong, nonatomic) NSString *bicycle;
@property (strong, nonatomic) NSString *bus;
@property (strong, nonatomic) NSString *car;
@property (strong, nonatomic) NSString *carexist;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *creditCard;
@property (strong, nonatomic) NSString *creditValidation;
@property (strong, nonatomic) NSString *licensec;
@property (strong, nonatomic) NSString *licensep;
@property (strong, nonatomic) NSString *moto;
@property (strong, nonatomic) NSString *other;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *points;
@property (strong, nonatomic) NSString *ship;
@property (strong, nonatomic) NSString *truck;
@property (strong, nonatomic) NSString *van;
@property (strong, nonatomic) NSURL *imageURL;
@property (nonatomic, strong) NSDictionary *allUsers;
@property (nonatomic, strong) NGUserInfo *aboutUser;
@end

@implementation NGUserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getAvatar];
    [self statusSwith];
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.aboutUser = [NGUserInfo new];
    self.aboutUser.role = [user objectForKey:@"role"];
    self.aboutUser.name = [user objectForKey:@"name"];
    self.aboutUser.surname = [user objectForKey:@"surname"];
    self.aboutUser.nickname = [user objectForKey:@"nickname"];
    self.aboutUser.birthdate = [user objectForKey:@"birthdate"];
    self.aboutUser.email = [user objectForKey:@"email"];
    self.aboutUser.status = [user objectForKey:@"status"];
    self.aboutUser.filePach = @"";
    self.aboutUser.admin_check = @"";
    self.aboutUser.card = @"";
    self.aboutUser.id_user = @"";
    self.aboutUser.denied = @"";
    self.aboutUser.rating = @"";
    self.aboutUser.address = @"";
    self.aboutUser.adminCheck = @"";
    self.aboutUser.bicycle = @"";
    self.aboutUser.bus = @"";
    self.aboutUser.car = @"";
    self.aboutUser.carexist = @"";
    self.aboutUser.city = @"";
    self.aboutUser.country = @"";
    
    self.aboutUser.creditCard = @"";
    self.aboutUser.creditValidation = @"";
    self.aboutUser.licensec = @"";
    self.aboutUser.licensep = @"";
    self.aboutUser.moto = @"";
    self.aboutUser.other = @"";
    self.aboutUser.phone = @"";
    self.aboutUser.points = @"";
    self.aboutUser.ship = @"";
    self.aboutUser.truck = @"";
    self.aboutUser.van = @"";
//    self.filePach = [user objectForKey:@"filePach"];
//    self.admin_check = [user objectForKey:@"admin_check"];
//    self.card = [user objectForKey:@"card"];
//    self.id_user = [user objectForKey:@"id_user"];
//    self.denied = [user objectForKey:@"denied"];
//    self.rating = [user objectForKey:@"rating"];
//    self.address = [user objectForKey:@"address"];
//    self.adminCheck = [user objectForKey:@"adminCheck"];
//    self.bicycle = [user objectForKey:@"bicycle"];
//    self.bus = [user objectForKey:@"bus"];
//    self.car = [user objectForKey:@"car"];
//    self.carexist = [user objectForKey:@"carexist"];
//    self.city = [user objectForKey:@"city"];
//    self.country = [user objectForKey:@"country"];
//    
//    self.creditCard = [user objectForKey:@"creditCard"];
//    self.creditValidation = [user objectForKey:@"creditValidation"];
//    self.licensec = [user objectForKey:@"licensec"];
//    self.licensep = [user objectForKey:@"licensep"];
//    self.moto = [user objectForKey:@"moto"];
//    self.other = [user objectForKey:@"other"];
//    self.phone = [user objectForKey:@"phone"];
//    self.points = [user objectForKey:@"points"];
//    self.ship = [user objectForKey:@"ship"];
//    self.truck = [user objectForKey:@"truck"];
//    self.van = [user objectForKey:@"van"];
    
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        //[myAlertView show];
        
    }
    
    
    
    self.imageUser.layer.cornerRadius = CGRectGetHeight(self.imageUser.bounds)/2;
    self.imageUser.layer.masksToBounds = YES;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 140; // tweak as needed
    const float movementDuration = 0.2f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void) getAvatar {
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *photo = [user objectForKey:@"photo"];
       NSString * server = @"http://pprapp.portaleb.com";
    NSString * avatar = [NSString stringWithFormat:@"%@%@", server, photo];
    NSURL* img;
    if (avatar) {
        img = [NSURL URLWithString:avatar];
    }
    [self.imageUser setImageWithURL:img placeholderImage:[UIImage imageNamed:@"userimg"]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)uploadPhoto:(id)sender {
    
    //[self updateAvatar];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageUser.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths firstObject];
    if (path) {
        self.filePach = [path stringByAppendingPathComponent:@"Image.png"];
        
        // Save image.
        [UIImagePNGRepresentation(chosenImage) writeToFile:self.filePach atomically:YES];
        
        [self updateAvatar];
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)takePhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}


- (void) updateAvatar {
    
    [[NGServerManager sharedManager]
     addAlbom:@"avatar"
     addFilePath:self.filePach
     
     onSuccess:^(NSDictionary *dict) {
         
         
         [self getAvatar];
         
         
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"Ohh" subTitle:@"You add bad login or password" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];

}

- (IBAction)saveChangesButton:(id)sender {


    
//    
//    NSDictionary *requestParametrs = [NSDictionary dictionaryWithObjectsAndKeys:
//                                      @"",       @"token",
//                                      self.name,                    @"user_name",
//                                      _surname,                 @"user_surname",
//                                      _nickname,                     @"nickname",
//                                      _birthdate,                    @"birthdate",
//                                      email,                        @"email",
//                                      id_fb,                        @"id_fb",
//                                      status,                       @"status",
//                                      role,                         @"role",
//                                      car,                          @"car",
//                                      address,                      @"address",
//                                      phone,                        @"phone",
//                                      creditCard,                   @"creditCard",
//                                      bicycle,                      @"bicycle",
//                                      moto,                         @"moto",
//                                      carexist,                     @"carexist",
//                                      van,                          @"van",
//                                      truck,                        @"truck",
//                                      bus,                          @"bus",
//                                      ship,                         @"ship",
//                                      licensep,                     @"licensep",
//                                      latitude,                     @"latitude",
//                                      longitude,                    @"longitude",
//                                      other,                        @"other",
//                                      country,                      @"country",
//                                      city,                         @"city",
//                                      nil];
//    
//
//    NSDictionary *dict = [EKSerializer serializeObject:self.aboutUser withMapping:[NGUserInfo objectMapping] ];
    
    [[NGServerManager sharedManager]
     
     addUser_name:self.aboutUser.name
     addUser_surname:self.aboutUser.surname
     addNickname:self.aboutUser.nickname
     addBirthdate:self.aboutUser.birthdate
     addEmail:self.aboutUser.email
     addId_fb:@""
     addStatus:self.aboutUser.status
     addRole:self.aboutUser.statusUser
     addCar:self.aboutUser.car
     addAddress:self.aboutUser.address
     addPhone:self.aboutUser.phone
     addCreditCard:self.aboutUser.creditCard
     addBicycle:self.aboutUser.bicycle
     addMoto:self.aboutUser.moto
     addCarexist:self.aboutUser.carexist
     addVan:self.aboutUser.van
     addTruck:self.aboutUser.truck
     addBus:self.aboutUser.bus
     addShip:self.aboutUser.ship
     addLicensep:self.aboutUser.licensep
     addLatitude:@"50.263500"
     addLongitude:@"38.268500"
     addOther:self.aboutUser.other
     addCountry:self.aboutUser.country
     addCity:self.aboutUser.city
     onSuccess:^(NSDictionary *dict) {
         
         NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
         NGUserInfo *userInfo = [EKMapper objectFromExternalRepresentation:dict[@"profile"] withMapping:[NGUserInfo objectMapping]];
//         userInfo.name;
//         userInfo.surname;
         [user setObject:userInfo forKey:@"userInfo"];
//         if (dict[@"profile"][@"photo"][@"url"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"photo"];
//         } else [user setObject:dict[@"profile"][@"photo"][@"url"] forKey:@"photo"];
//         if (dict[@"profile"][@"admin_check"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"admin_check"];
//         } else [user setObject:dict[@"profile"][@"admin_check"] forKey:@"admin_check"];
//         if (dict[@"profile"][@"card"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"card"];
//         } else [user setObject:dict[@"profile"][@"card"] forKey:@"card"];
//         if (dict[@"profile"][@"id_user"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"id_user"];
//         } else [user setObject:dict[@"profile"][@"id_user"] forKey:@"id_user"];
//         if (dict[@"profile"][@"email"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"email"];
//         } else [user setObject:dict[@"profile"][@"email"] forKey:@"email"];
//         if (dict[@"profile"][@"denied"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"denied"];
//         } else [user setObject:dict[@"profile"][@"denied"] forKey:@"denied"];
//         if (dict[@"profile"][@"birthdate"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"birthdate"];
//         } else [user setObject:dict[@"profile"][@"birthdate"] forKey:@"birthdate"];
//         if (dict[@"profile"][@"nickname"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"nickname"];
//         } else [user setObject:dict[@"profile"][@"nickname"] forKey:@"nickname"];
//         if (dict[@"profile"][@"role"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"role"];
//         } else [user setObject:dict[@"profile"][@"role"] forKey:@"role"];
//         if (dict[@"profile"][@"rating"][@"value"] == [NSNull null]) {
//             [user setObject:@"0" forKey:@"rating"];
//         } else [user setObject:dict[@"profile"][@"rating"][@"value"] forKey:@"rating"];
//         if (dict[@"profile"][@"name"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"name"];
//         } else [user setObject:dict[@"profile"][@"name"] forKey:@"name"];
//         if (dict[@"profile"][@"status"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"status"];
//         } else [user setObject:dict[@"profile"][@"status"] forKey:@"status"];
//         if (dict[@"profile"][@"surname"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"surname"];
//         } else [user setObject:dict[@"profile"][@"surname"] forKey:@"surname"];
//         if (dict[@"profile"][@"address"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"address"];
//         } else [user setObject:dict[@"profile"][@"address"] forKey:@"address"];
//         if (dict[@"profile"][@"adminCheck"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"adminCheck"];
//         } else [user setObject:dict[@"profile"][@"adminCheck"] forKey:@"adminCheck"];
//         if (dict[@"profile"][@"bicycle"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"bicycle"];
//         } else [user setObject:dict[@"profile"][@"bicycle"] forKey:@"bicycle"];
//         if (dict[@"profile"][@"bus"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"bus"];
//         } else [user setObject:dict[@"profile"][@"bus"] forKey:@"bus"];
//         if (dict[@"profile"][@"car"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"car"];
//         } else [user setObject:dict[@"profile"][@"car"] forKey:@"car"];
//         if (dict[@"profile"][@"carexist"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"carexist"];
//         } else [user setObject:dict[@"profile"][@"carexist"] forKey:@"carexist"];
//         if (dict[@"profile"][@"city"] == [NSNull null]) {
//             [user setObject:@"0" forKey:@"city"];
//         } else [user setObject:dict[@"profile"][@"city"] forKey:@"city"];
//         if (dict[@"profile"][@"country"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"country"];
//         } else [user setObject:dict[@"profile"][@"country"] forKey:@"country"];
//         if (dict[@"profile"][@"creditCard"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"creditCard"];
//         } else [user setObject:dict[@"profile"][@"creditCard"] forKey:@"creditCard"];
//         if (dict[@"profile"][@"creditValidation"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"creditValidation"];
//         } else [user setObject:dict[@"profile"][@"creditValidation"] forKey:@"creditValidation"];
//         if (dict[@"profile"][@"licensec"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"licensec"];
//         } else [user setObject:dict[@"profile"][@"licensec"] forKey:@"licensec"];
//         if (dict[@"profile"][@"licensep"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"licensep"];
//         } else [user setObject:dict[@"profile"][@"licensep"] forKey:@"licensep"];
//         if (dict[@"profile"][@"moto"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"moto"];
//         } else [user setObject:dict[@"profile"][@"moto"] forKey:@"moto"];
//         if (dict[@"profile"][@"other"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"other"];
//         } else [user setObject:dict[@"profile"][@"other"] forKey:@"other"];
//         if (dict[@"profile"][@"phone"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"phone"];
//         } else [user setObject:dict[@"profile"][@"phone"] forKey:@"phone"];
//         if (dict[@"profile"][@"points"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"points"];
//         } else [user setObject:dict[@"profile"][@"points"] forKey:@"points"];
//         if (dict[@"profile"][@"ship"] == [NSNull null]) {
//             [user setObject:@"0" forKey:@"ship"];
//         } else [user setObject:dict[@"profile"][@"ship"] forKey:@"ship"];
//         if (dict[@"profile"][@"truck"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"truck"];
//         } else [user setObject:dict[@"profile"][@"truck"] forKey:@"truck"];
//         if (dict[@"profile"][@"van"] == [NSNull null]) {
//             [user setObject:@"" forKey:@"van"];
//         } else [user setObject:dict[@"profile"][@"van"] forKey:@"van"];
     
         [user synchronize];
         
         NSString* role = dict[@"profile"][@"role"];
         NSString* object = dict[@"object"];
         
         if ([object isEqualToString:@"error"]) {
             [self statusSwith];
             
             [user synchronize];
             SCLAlertView *alert = [[SCLAlertView alloc] init];
             [alert showError:self title:@"Ohh" subTitle:object closeButtonTitle:@"OK" duration:0.0f];
         }
         else {
             
             if ([self.role isEqualToString:role]) {
//                 SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
//                 
//                 [alert showSuccess:@"" subTitle:@"Save" closeButtonTitle:@"Done" duration:3.0f];
             }
             
             
             if ([role isEqualToString:@"p"]) {
                 
                 SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                 
                 [alert showSuccess:@"Save" subTitle:@"You Provider" closeButtonTitle:@"Done" duration:3.0f];
                 
                 NSString * storyboardName = @"Provider";
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                 UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"RevealVC"];
                 [self.navigationController presentViewController:vc animated:YES completion:nil];
                 
             }
             else {
                 
                 SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                 
                 [alert showSuccess:@"Save" subTitle:@"You Creator" closeButtonTitle:@"Done" duration:3.0f];
                 
                 NSString * storyboardName = @"Creator";
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                 UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"RevealVC"];
                 //[self showViewController:vc sender:self];
                 [self.navigationController presentViewController:vc animated:YES completion:nil];
             
             
             }
         }
    
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         [self statusSwith];
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"Ohh" subTitle:@"You add bad login or password" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];
    

    
}
- (void) statusSwith {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.role = [user objectForKey:@"role"];
    
    if ([self.role isEqualToString: @"p"]) {
        self.labelStatusUser.text = @"YES";
        [self.switchUser setOn:YES animated:YES];
    }
    else {
        self.labelStatusUser.text = @"NO";
        self.statusUser = @"c";
        [self.switchUser setOn:NO animated:YES];
    }
    
}

- (IBAction)changeStatusUserSwitch:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    if (self.switchUser.on) {
     self.labelStatusUser.text = @"YES";
        self.statusUser = @"p";
        //[user setObject:@"p" forKey:@"role"];
    }
    else {
        self.labelStatusUser.text = @"NO";
        self.statusUser = @"c";
        //[user setObject:@"c" forKey:@"role"];
    }
    
    [user synchronize];
    
}
@end
