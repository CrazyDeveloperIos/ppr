//
//  NGMyMissionViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 5/29/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGMyMissionViewController.h"
#import "NGViewControllerMapTable.h"
#import "NGServerManager.h"
#import "NGMissionData.h"
#import <QuartzCore/QuartzCore.h>
#import "SCLAlertView.h"
#import "NGServetObject.h"
#import "NGCustomUserAcceptVC.h"
#import "SWRevealViewController.h"

@implementation NGMyMissionViewController


#pragma mark - API

- (void)viewDidLoad {
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        self.sidebarButton.action = @selector(revealToggle:);
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    self.idMission = @"";
    self.sort = @"distance";
    [self getMission];
    self.missionsArray = [NSMutableArray new];
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;

    self.refreshView = [[UIRefreshControl alloc] init];
    
    [self.refreshView addTarget:self action:@selector(getMission) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshView];

    
}

- (IBAction)filter:(id)sender {
    switch (((UISegmentedControl *)sender).selectedSegmentIndex) {
        case 0:
            self.sort =@"distance";
            [self getMission];
            [self.tableView reloadData];
            break;
            
        case 1:
            self.sort =@"date";
            [self getMission];
            [self.tableView reloadData];
            break;
        default:
            break;
    }
}
- (IBAction)runMission:(id)sender {
    
}


- (IBAction)showUserAcceptedMission:(id)sender{
    NGCustomUserAcceptVC *acceptedUserVC =
    [self.storyboard instantiateViewControllerWithIdentifier:@"acceptedUserVC"];
    [self.navigationController presentViewController:acceptedUserVC animated:YES completion:nil];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.idMission forKey:@"id"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedMissionId"
                                                        object:self.idMission
                                                      userInfo:userInfo];

    
    
}

-(void) getMission {
    [[NGServerManager sharedManager]
     
     addCreator:@"22"
     addProvider:@"0"
     addNum:@"1"
     addPerpage:@"100"
     addSort_flow:@"0"
     addSort_by:self.sort
     addUser_latitude: @"51.2344800"
     addUser_longitude:@"28.6759930"
     addDis:@"300000"
     addDis_cut:@"0"
     addstatusM:@"n"
     
     
     onSuccess:^(NSDictionary *dict) {
         [self.missionsArray removeAllObjects];
         NSArray *missions = dict[@"missionParty"];
         for (NSDictionary *mission in missions){
             NGMissionData *missionData = [[NGMissionData alloc] initWithServerResponse:mission];
             [self.missionsArray addObject:missionData];
         }
         self.allUsers = dict[@"usersShort"];
         [self.tableView reloadData];
         [self.refreshView endRefreshing];
     }
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
         [self.refreshView endRefreshing];
         
         SCLAlertView *alert = [[SCLAlertView alloc] init];
         [alert showError:self title:@"" subTitle:@"Not internet connection" closeButtonTitle:@"OK" duration:0.0f]; // Error
         
     }];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.missionsArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NGMissionData *mission = self.missionsArray[indexPath.row];
    int aValue = [[mission.time_start stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    int distanse = [[mission.distanse stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    
    NSInteger TimeStamp = aValue;
    
    NSDate *messageDatea = [NSDate dateWithTimeIntervalSince1970:TimeStamp];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    NSString *dateString = [dateFormatter stringFromDate:messageDatea];
    
    NSString *integerAsString = [@(distanse ) stringValue];
    NSString *distance = [NSString stringWithFormat:@"%@ %@ %@", @"at",integerAsString, @"km"];
    
    static NSString *standartIdentifier = @"standartCell";
    static NSString *advancedIdentifier = @"advancedCell";
    
    if (indexPath == self.selectedCell){
        NGCustomTableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:advancedIdentifier];
        if(!cell){
            cell = [[NGCustomTableViewCell alloc] initWithStyle:
                    UITableViewCellStyleDefault      reuseIdentifier:advancedIdentifier];
        }
        UIView *view=[[UIView alloc]init];
        UIButton *addButton=[UIButton buttonWithType:UIButtonTypeContactAdd];
        addButton.frame=CGRectMake(250, 0, 100, 50);
        [view addSubview:addButton];
        [tableView.tableHeaderView insertSubview:view atIndex:0];
        
       
        self.idMission = mission.id_mission;
        int aValue = [[mission.time_start stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
        int andValue = [[mission.time_end stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
        
        NSInteger TimeStamp = aValue;
        NSInteger andTimeStamp = andValue;
        
        NSDate *messageDatea = [NSDate dateWithTimeIntervalSince1970:TimeStamp];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        
        NSString *dateString = [dateFormatter stringFromDate:messageDatea];
        
        NSDate *andMessageDatea = [NSDate dateWithTimeIntervalSince1970:andTimeStamp];
        NSDateFormatter* anddateFormatter = [[NSDateFormatter alloc] init];
        
        [anddateFormatter setDateStyle:NSDateFormatterMediumStyle];
        
        NSString *andDateString = [dateFormatter stringFromDate:andMessageDatea];
        
        cell.distancionLabel.text = distance;
        cell.distancLabel.text = distance;
        cell.dateLabel.text = dateString;
        cell.dateRealizationLabel.text = andDateString;
        cell.userLabel.text = self.allUsers[mission.creator][@"nickname"];
        
        
        return cell;
    }
    NGCustomTableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:standartIdentifier];
    if(!cell){
        cell = [[NGCustomTableViewCell alloc] initWithStyle:
                UITableViewCellStyleDefault      reuseIdentifier:standartIdentifier];
    }
    cell.distancionLabel.text = distance;
    cell.dateMission.text = dateString;
    cell.descriptionLabel.text = mission.descriptionOne;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

if (self.selectedCell == indexPath){
        self.selectedCell = nil;
        [self.linksTableView reloadData];
        return;
    }
//    [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];
    
    [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationAutomatic];
    
//    
//    [UIView animateWithDuration:0.2 animations:^{
//    
//        [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationAutomatic];
//
//        
//    }];
    
    self.selectedCell = indexPath;
    [self.linksTableView reloadData];
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath == self.selectedCell){
        
        return 240;
    }
    return 80;
}


@end
