//
//  NGMessageViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 6/16/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGMessageViewController.h"
#import "NGServerManager.h"
#import "AFNetworking.h"
#import "NGAccessToken.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"
#import "NGCustomDialogTableViewCell.h"
#import "DemoMessagesViewController.h"
#import "NGMessageData.h"
#import "SWRevealViewController.h"


@interface NGMessageViewController ()
@property (strong, nonatomic) NSArray *googlePlacesArrayFromAFNetworking;
@property (strong, nonatomic) NSString *textResponce;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSArray* messageParty;
@property (strong, nonatomic) NSMutableArray* arrayMessage;
@property (strong, nonatomic) NSDictionary* userParty;
@property (nonatomic, strong) NSDictionary *allUsers;
@property (strong, nonatomic) NSDictionary* user;
@property (strong, nonatomic) id obj;
@property (nonatomic ,strong) UIRefreshControl *refreshView;
@property (strong, nonatomic) NGMessageData *message;


@end

@implementation NGMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        self.sidebarButton.action = @selector(revealToggle:);
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.arrayMessage = [NSMutableArray new];
    [self getDialogUser];
    
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;
    self.refreshView = [[UIRefreshControl alloc] init];
    
    [self.refreshView addTarget:self action:@selector(getDialogUser) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshView];
}

-(void)viewWillAppear:(BOOL)animated{
     [super viewWillAppear:animated];
     //[self.navigationController setNavigationBarHidden:NO animated:YES];

     [self getDialogUser];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getDialogUser {
    
        [[NGServerManager sharedManager]
    
         addNum:    @"1"
         addPerpage:@"4"
    
         onSuccess:^(NSDictionary *dict) {
             
             [self.arrayMessage removeAllObjects];
             
             self.messageParty =  dict[@"messageParty"];
             for (NSDictionary *messag in self.messageParty){
                 NGMessageData *messageData = [[NGMessageData alloc] initWithServerResponse:messag];
                 [self.arrayMessage addObject:messageData];
             }
             self.allUsers = dict[@"userParty"];

    
             self.obj = [self.messageParty firstObject];
             self.userParty =  dict[@"userParty"];
             self.userID =  [self.obj[@"from"] stringValue];
             self.user = self.userParty[self.userID];
             
             self.textResponce = dict[@"text"];
             self.textResponce = dict[@"action"];
    
             NSLog(@"%@",self.textResponce);
             
             [self.tableView reloadData];
             
             [self.refreshView endRefreshing];

    
         }
         onFailure:^(NSError *error) {
    
             NSLog(@"ERROR: %@", error);
    
             SCLAlertView *alert = [[SCLAlertView alloc] init];
             [alert showError:self title:@"Ohh" subTitle:@"Bab data" closeButtonTitle:@"OK" duration:0.0f]; // Error
             
         }];
    
    }

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMessage.count;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.message = self.arrayMessage[indexPath.row];
    static NSString *simpleTableIdentifier = @"Cell";
        NGCustomDialogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    if(!cell){
        cell = [[NGCustomDialogTableViewCell alloc] initWithStyle:
                UITableViewCellStyleDefault      reuseIdentifier:simpleTableIdentifier];
    }
   // NSInteger TimeStamp = (NSInteger) self.message.time_create;
    
    //NSDate *messageDatea = [NSDate dateWithTimeIntervalSince1970:TimeStamp];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
   // NSString *dateString = [dateFormatter stringFromDate:messageDatea];
    
    NSString *idUs = [NSString stringWithFormat:@"%@", self.message.from];
    cell.timeLabel.text = @"today";
    cell.nameUser.text = [self.allUsers objectForKey:(idUs)][@"nickname"];
    cell.textMessage.text = self.message.text;

    NSString* userAvatar = [self.allUsers objectForKey:(idUs)][@"photo"][@"url"];
    //self.idSelectedMission = mission.id_mission;
    NSURL *imageURL;
    NSString * server = @"http://agent1.kievregion.net";
    NSString * avatar = [NSString stringWithFormat:@"%@%@", server, userAvatar];
    if (avatar) {
        imageURL = [NSURL URLWithString:avatar];
    }
    [cell.userImage setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"userimg"]];

    return cell;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    self.message = self.arrayMessage[indexPath.row];
    return indexPath;
}
- (void) getFullMessageInGialog {
    
    [[NGServerManager sharedManager]
     
     addidDialog:self.message.id_dialog
     addIdMessage:self.message.id_msg
     addPage:@"10"
     
     onSuccess:^(NSDictionary *dict) {
         self.textResponce = dict[@"text"];
         self.textResponce = dict[@"action"];
         //NSLog((NSData*)self.textResponce);
         
     }
     
     onFailure:^(NSError *error) {
         
         NSLog(@"ERROR: %@", error);
         
     }];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toMessageItem"]){
        DemoMessagesViewController *vc = (DemoMessagesViewController *)[segue destinationViewController];
        vc.id_msg = self.message.id_msg;
        vc.id_dialog = self.message.id_dialog;
    }
    
    
}

@end
