//
//  NGCreateNewMissionViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGGeocodingService.h"
#import <GoogleMaps/GoogleMaps.h>
#import "NGMissionData.h"

@protocol  CreateNewMissionProtocol;


@interface NGCreateNewMissionViewController : UIViewController <UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) id <CreateNewMissionProtocol> delegate;

@end

@protocol CreateNewMissionProtocol <NSObject>

- (void)createNewMissionDissmiss;

@end