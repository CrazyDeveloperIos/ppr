//
//  ViewController.m
//  Uni Bit
//
//  Created by Nazar Gorobets on 4/5/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "NGStartViewController.h"
#import "NGViewControllerMapTable.h"
#import "NGServerManager.h"
#import "SCLAlertView.h"
#import "SWRevealViewController.h"
#import "SidebarViewController.h"
#import "AMTumblrHud.h"
#import <MRProgress/MRProgressOverlayView+AFNetworking.h>

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface NGStartViewController ()<FBSDKLoginTooltipViewDelegate, FBSDKLoginButtonDelegate>

@end

@implementation NGStartViewController

- (void)viewDidLoad {
//    [self getStatusUserInApp];
    [super viewDidLoad];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - connectionButton
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range     replacementString:(NSString *)string
{
    if (textField.text.length >= 40 && range.length == 0)
        return NO;
    return YES;
}

- (IBAction)connectionButton:(id)sender { 
    if (self.emalTextField.text.length != 0 || self.passwordTextField.text.length != 0) {
        
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        
        // saving an NSString
        [user setObject:self.emalTextField.text forKey:@"userName"];
        [user setObject:self.passwordTextField.text forKey:@"password"];
        
        [user synchronize];
        
        NSString *userName = [user objectForKey:@"userName"];
        NSString *password = [user objectForKey:@"password"];
        [[NGServerManager sharedManager]
         withLoginUser:userName
         andPasswordUser:password
         onSuccess:^(NSDictionary *dict) {
             //[tumblrHUD hide];
             [user setObject:dict[@"user"][@"admin_check"] forKey:@"admin_check"];
             [user setObject:dict[@"user"][@"card"] forKey:@"card"];
             [user setObject:dict[@"user"][@"denied"] forKey:@"denied"];
             [user setObject:dict[@"user"][@"latitude"] forKey:@"latitude"];
             [user setObject:dict[@"user"][@"longitude"] forKey:@"longitude"];
             //[user setObject:dict[@"user"][@"photo"][@"url"] forKey:@"photo"];
             [user setObject:dict[@"user"][@"id_user"] forKey:@"id_user"];
             [user setObject:dict[@"user"][@"email"] forKey:@"email"];
             [user setObject:dict[@"user"][@"birthdate"] forKey:@"birthdate"];
             [user setObject:dict[@"user"][@"nickname"] forKey:@"nickname"];
             [user setObject:dict[@"user"][@"role"] forKey:@"role"];
             //[user setObject:dict[@"user"][@"rating"][@"value"] forKey:@"rating"];
             [user setObject:dict[@"user"][@"token"] forKey:@"token"];
             [user setObject:dict[@"user"][@"name"] forKey:@"name"];
             [user setObject:dict[@"user"][@"status"] forKey:@"status"];
             [user setObject:dict[@"user"][@"surname"] forKey:@"surname"];
             [user setObject:dict[@"user"][@"token"] forKey:@"token"];
             
             [user synchronize];
             
             NSString* role = dict[@"user"][@"role"];
             NSString* object = dict[@"object"];
             
             if ([object isEqualToString:@"error"]) {
                 NSLog(@"error");
             }
             else {
                 if ([role isEqualToString:@"p"]) {
                     
                     NSString * storyboardName = @"Provider";
                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                     UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"RevealVC"];
                     [self showViewController:vc sender:self];
                     
                 }
                 else {
                     
                     NSString * storyboardName = @"Creator";
                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                     UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"RevealVC"];
                     [self showViewController:vc sender:self];
                 }
             }
             
             
         }
         
         onFailure:^(NSError *error) {
             //[tumblrHUD showAnimated:NO];
             SCLAlertView *alert = [[SCLAlertView alloc] init];
             [alert showError:self title:@"Oh!" subTitle:@"Please, check your Inretnet connection " closeButtonTitle:@"OK" duration:0.0f]; // Error
             //[self getStatusUserInApp];
             NSLog(@"ERROR: %@", error);
         }];
        
        
    }
    else {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Oh!" subTitle:@"You need add login and password" closeButtonTitle:@"OK" duration:0.0f]; // Error
    }
}



#pragma mark - loginButtonFacebook

- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{
    
    if ([FBSDKAccessToken currentAccessToken]) {
        NSString *fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"  parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 SCLAlertView *alert = [[SCLAlertView alloc] init];
                 
                 [alert showSuccess:self title:([result objectForKey:@"name"]) subTitle:@"Login and Password send to you email" closeButtonTitle:@"Ok" duration:8.0f];
                 
                 NSLog(@"email is %@", [result objectForKey:@"email"]);
//
                 [[NGServerManager sharedManager]
                  
                  withLoginUserFacebook :fbAccessToken
                  
                  onSuccess:^(NSDictionary *dict) {
                      
                      SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                      
                      
                      [alert showSuccess:([result objectForKey:@"name"]) subTitle:@"Login and Password send to you email" closeButtonTitle:@"" duration:4.0f];
                      
                  }
                  onFailure:^(NSError *error) {
                      
                      NSLog(@"ERROR: %@", error);
                      
                      SCLAlertView *alert = [[SCLAlertView alloc] init];
                      [alert showError:self title:@"Oh!" subTitle:@"Please, check your Inretnet connection " closeButtonTitle:@"OK" duration:0.0f]; // Error
                      NSLog(@"ERROR: %@", error);
                      
                  }];
                 
             }
             else{
                 NSLog(@"%@", [error localizedDescription]);
             }
         }];
    }
    else {
        NSLog(@"Error");
    }
    
}

#pragma mark - loginButtonDidLogOutFacebook

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
    [[NGServerManager sharedManager] withClickLogout:nil
     
                                           onSuccess:^(NSDictionary *dict) {
                                               
                                           }
                                           onFailure:^(NSError *error) {
                                               
                                                      SCLAlertView *alert = [[SCLAlertView alloc] init];
                                               [alert showError:self title:@"Oh!" subTitle:@"Please, check your Inretnet connection " closeButtonTitle:@"OK" duration:0.0f]; // Error
                                               NSLog(@"ERROR: %@", error);
                                               
                                           }];
    
    
}

#pragma mark - prepareForSegue

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if ([segue.identifier isEqualToString:@"register"]) {
//        [UIView animateWithDuration:0.8
//                              delay:0
//                            options:UIViewAnimationOptionTransitionFlipFromBottom
//                         animations:^{
//                             [UIView setAnimationTransition:
//                              UIViewAnimationTransitionFlipFromRight
//                                                    forView:self.navigationController.view cache:NO];
//
//                         }
//                         completion:^(BOOL finished){
//
//                         }];
//    }
//}
@end