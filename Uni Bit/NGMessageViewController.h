//
//  NGMessageViewController.h
//  Uni Bit
//
//  Created by Nazar Gorobets on 6/16/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGMessageViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *linksTableView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
