//
//  SidebarViewController.m
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "NGCreateNewMissionViewController.h"
#import "UIImageView+AFNetworking.h"


@interface SidebarViewController ()


@end


@implementation SidebarViewController {

    NSArray *menuItems;
    __weak IBOutlet UIImageView *imageUser;
    __weak IBOutlet UILabel *nameUser;
}
@synthesize imageUser;
@synthesize nameUser;
@dynamic tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *role = [user objectForKey:@"role"];
   // NSString *photo = [user objectForKey:@"photo"];
    NSString *nick = [user objectForKey:@"nickname"];
    if ([role isEqualToString: @"p"]) {
        
        self.roleUser.text = @"Provider";
        self.nameUser.text = nick;
        menuItems = @[@"Book Mission",@"My Mission", @"Register as Driver", @"Support", @"Setting"];
    }
    else {
            menuItems = @[@"Book Mission",@"My Mission", @"Payments", @"Get Discount", @"Register as Driver", @"Support", @"Setting"];
        self.roleUser.text = @"Creator";
        self.nameUser.text = nick;

    }
    
    self.nameUser.text = nick;

    self.imageUser.layer.cornerRadius = CGRectGetHeight(imageUser.bounds)/2;
    self.imageUser.layer.masksToBounds = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *photo = [user objectForKey:@"photo"];
    NSURL *imageURL;
    NSString * server = @"http://pprapp.portaleb.com";
    NSString * avatar = [NSString stringWithFormat:@"%@%@", server, photo];
    if (avatar) {
        imageURL = [NSURL URLWithString:avatar];
    }
    

                        [self.imageUser setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"userimg"]];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
// Set the title of navigation bar by using the menu items
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
    
    // Set the photo if it navigates to the PhotoView
    if ([segue.identifier isEqualToString:@"showPhoto"]) {
 }
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
}

@end
