//
//  NGLeftPanelViewController.m
//  MCPanelViewController
//
//  Created by Matthew Cheok on 3/10/13.
//  Copyright (c) 2013 Matthew Cheok. All rights reserved.
//

#import "NGLeftPanelViewController.h"
#import "NGViewControllerMapTable.h"
#import "NGCreateNewMissionViewController.h"


@interface NGLeftPanelViewController ()

@end

@implementation NGLeftPanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithWhite:0.333 alpha:0.3];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if( [self.delegate respondsToSelector:@selector(menuItemSelected:)]){
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath]
        ;
        [self.delegate menuItemSelected:indexPath.row];
    }
    
}


@end
