//
//  NGSupportViewController.h
//  Uni Bit
//
//  Created by Naz on 12.08.15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGSupportViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@end
