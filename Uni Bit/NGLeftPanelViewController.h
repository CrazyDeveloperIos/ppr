//
//  NGLeftPanelViewController.h
//  MCPanelViewController
//
//  Created by Matthew Cheok on 3/10/13.
//  Copyright (c) 2013 Matthew Cheok. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    Map,
    Create,
    MyWork,
    Messages,
    Settings
 }MenuSections;

@protocol NGLeftMenuDelegate;
@interface NGLeftPanelViewController : UITableViewController
@property (nonatomic, weak) id<NGLeftMenuDelegate> delegate;
@end


@protocol NGLeftMenuDelegate <NSObject>
@required
-(void)menuItemSelected:(MenuSections) section;
@end