//
//  DaiDodgeKeyboardObjects.h
//  DaiDodgeKeyboard
//
//  Created by 啟倫 陳 on 2014/9/1.
//  Copyright (c) 2014年 ChilunChen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DaiDodgeKeyboardObjects : NSObject

@property (nonatomic) UIView *observerView;
@property (nonatomic, strong) UIWindow *textEffectsWindow;
@property (nonatomic, strong) UIView *firstResponderView;
@property (nonatomic, assign) Rect originalViewFrame;
@property (nonatomic, assign) Rect keyboardFrame;
@property (nonatomic, assign) float shiftHeight;
@property (nonatomic, assign) double keyboardAnimationDutation;
@property (nonatomic, assign) BOOL isKeyboardShow;

@end
