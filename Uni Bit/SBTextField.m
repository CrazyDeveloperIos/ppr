//
//  SBTextField.m
//  SBTextField
//
//  Created by Scott Berrevoets on 6/18/13.
//  Copyright (c) 2013 Scotty Doesn't Code. All rights reserved.
//

#import "SBTextField.h"

/*! A class that acts as the intermediary delegate for an SBTextField instance. It passes UITextField messages directly to what the user defines as the delegate, but it makes sure that (if set) the max length will not be exceeded.*/
@interface SBTextFieldDelegate : NSObject <UITextFieldDelegate>
/*! The real delegate for a text field, defined by the developer */
@property (nonatomic, weak) id <UITextFieldDelegate> realDelegate;
@end

@interface SBTextField ()
/*! The intermediary delegate for the text field that receives all delegate messages first before passing them on.*/
@property (nonatomic, strong) SBTextFieldDelegate *intermediaryDelegate;
@end

@implementation SBTextField

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
	if (self)
		[self commonInit];
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	
	self = [super initWithCoder:aDecoder];
	
	if (self)
		[self commonInit];
	
	return self;
}

- (void)commonInit {
	self.intermediaryDelegate = [[SBTextFieldDelegate alloc] init];
	
	// Set the text field's delegate to the intermediary delegate. We have to call super, because self overrides setDelegate: (see below).
	[super setDelegate:self.intermediaryDelegate];
}

- (void)setDelegate:(id<UITextFieldDelegate>)delegate {
	// Set super's delegate to the intermediary delegate...
	[super setDelegate:self.intermediaryDelegate];
	
	// ... and set the intermediary's realDelegate to what the user actually wants to be the delegate
	self.intermediaryDelegate.realDelegate = delegate;
}

- (id<UITextFieldDelegate>)delegate {
	return self.intermediaryDelegate.realDelegate;
}

@end

@implementation SBTextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	if ([self.realDelegate respondsToSelector:@selector(textFieldShouldBeginEditing:)])
		return [self.realDelegate textFieldShouldBeginEditing:textField];
	else
		return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if ([self.realDelegate respondsToSelector:@selector(textFieldDidBeginEditing:)])
		[self.realDelegate textFieldDidBeginEditing:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
	if ([self.realDelegate respondsToSelector:@selector(textFieldShouldEndEditing:)])
		return [self.realDelegate textFieldShouldEndEditing:textField];
	else
		return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	if ([self.realDelegate respondsToSelector:@selector(textFieldDidEndEditing:)])
		[self.realDelegate textFieldDidEndEditing:textField];
}

- (NSInteger)maxLengthForTextField:(UITextField *)textField {
	SEL selector = @selector(maxLength);
	
	NSMethodSignature *methodSignature = [[textField class] instanceMethodSignatureForSelector:selector];
	NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:methodSignature];
	[invocation setSelector:selector];
	[invocation invokeWithTarget:textField];
	
	NSInteger maxLength = 0;
	[invocation getReturnValue:&maxLength];
	return maxLength;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	if ([textField respondsToSelector:@selector(maxLength)]) {
		NSUInteger oldLength = [textField.text length];
		NSUInteger replacementLength = [string length];
		NSUInteger rangeLength = range.length;
		
		NSUInteger newLength = oldLength - rangeLength + replacementLength;
		
		NSUInteger maxLength = [self maxLengthForTextField:textField];
		
		if (newLength > maxLength && [string rangeOfString:@"\n"].location == NSNotFound)
			return NO;
	}
	
	if ([self.realDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)])
		return [self.realDelegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
	else
		return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
	if ([self.realDelegate respondsToSelector:@selector(textFieldShouldClear:)])
		return [self.realDelegate textFieldShouldClear:textField];
	else
		return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if ([self.realDelegate respondsToSelector:@selector(textFieldShouldReturn:)])
		return [self.realDelegate textFieldShouldReturn:textField];
	else
		return YES;
}

@end
