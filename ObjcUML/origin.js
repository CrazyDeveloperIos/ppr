   var dependencies = {
    links:
      [
            { "source" : "AppDelegate", "dest" : "CLLocationManager" },
            { "source" : "AppDelegate", "dest" : "FBSDKAppEvents" },
            { "source" : "AppDelegate", "dest" : "FBSDKApplicationDelegate" },
            { "source" : "AppDelegate", "dest" : "FBSDKLoginButton" },
            { "source" : "AppDelegate", "dest" : "GMSServices" },
            { "source" : "main", "dest" : "AppDelegate" },
            { "source" : "NGCreateNewMissionViewController", "dest" : "CLGeocoder" },
            { "source" : "NGCreateNewMissionViewController", "dest" : "CLLocation" },
            { "source" : "NGCreateNewMissionViewController", "dest" : "GMSMarker" },
            { "source" : "NGCreateNewMissionViewController", "dest" : "NGServerManager" },
            { "source" : "NGCreateNewMissionViewController", "dest" : "SCLAlertView" },
            { "source" : "NGForgetPass", "dest" : "NGServerManager" },
            { "source" : "NGForgetPass", "dest" : "SCLAlertView" },
            { "source" : "NGRegisterViewController", "dest" : "NGServerManager" },
            { "source" : "NGRegisterViewController", "dest" : "SCLAlertView" },
            { "source" : "NGSettingViewController", "dest" : "FBSDKAccessToken" },
            { "source" : "NGSettingViewController", "dest" : "FBSDKGraphRequest" },
            { "source" : "NGSettingViewController", "dest" : "NGServerManager" },
            { "source" : "NGSettingViewController", "dest" : "SCLAlertView" },
            { "source" : "NGStartViewController", "dest" : "FBSDKAccessToken" },
            { "source" : "NGStartViewController", "dest" : "FBSDKGraphRequest" },
            { "source" : "NGStartViewController", "dest" : "NGServerManager" },
            { "source" : "NGStartViewController", "dest" : "SCLAlertView" },
            { "source" : "NGViewControllerMapTable", "dest" : "GMSMarker" },
            { "source" : "NGViewControllerMapTable", "dest" : "NGCustomTableViewCell" },
         ]
    }
  ;
